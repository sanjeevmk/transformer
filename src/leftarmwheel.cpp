#include "../includes/parts.h"

void LeftArmWheel::setup(SinglePivot p, const char *texture){
	parent = p;
	parentMatrix = parent.getModelMatrix();
	vec3 parentPos = vec3(parentMatrix[3]);
	
	leftarmwheel.createCylinder(radius,height,parentPos+offsetFromParent,texture);
	position = vec3(leftarmwheel.modelMatrix[3]);

	cylcomponents.push_back(leftarmwheel);	
}

void LeftArmWheel::render(){
	commonCylinderRender();
}

void LeftArmWheel::readjust(mat4 parentMatrix){
	mat4 newModel = translate(parentMatrix,offsetFromParent);
	newModel = scale(newModel,vec3(radius,radius,height));
	cylcomponents[0].modelMatrix = newModel;
}
