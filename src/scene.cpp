#include "../includes/scene.h"
#include "../includes/loadbmp.h"
#include <vector>

using namespace std;

void Skybox::setup() {
	const char* back = "../images/sincity_bk.bmp";
	const char* front = "../images/sincity_ft.bmp";
	const char* up = "../images/sincity_up.bmp";
	const char* down = "../images/sincity_dn.bmp";
	const char* left = "../images/sincity_lf.bmp";
	const char* right = "../images/sincity_rt.bmp";
	
	vector<const char*> imagepaths;

	imagepaths.push_back(right);
	imagepaths.push_back(left);
	imagepaths.push_back(down);
	imagepaths.push_back(up);
	imagepaths.push_back(back);
	imagepaths.push_back(front);

	vector<imageData> imgData = loadCubeMapImage(imagepaths);
	skybox.createSkyboxCuboid(width,height,depth,imgData);
}

void Skybox::render() {
	skybox.renderSkyboxCuboid();		
}

void Skybox::translate(vec3 trans){
	mat4 unscale = scale(skybox.modelMatrix,vec3(1.0f/width,1.0f/height,1.0f/depth));
	mat4 newModel = glm::translate(unscale,trans);
	newModel = scale(newModel,vec3(width,height,depth));
	skybox.modelMatrix = newModel;
}
