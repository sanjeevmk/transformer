#include "../includes/models.h"
#include "../includes/loadbmp.h"

#include <cmath>

#define PI 3.14159265359
#define toradians(x) (PI/180.0)*x

void unitCube::setupCubeMap() {
	glActiveTexture(GL_TEXTURE0);
	glEnable(GL_TEXTURE_CUBE_MAP);
	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_CUBE_MAP, texture);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR); 
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
}

void unitCube::setupCubeMap(vector<imageData> imgData ) {
	setupCubeMap();
	glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X, 0, GL_RGBA, imgData[0].width ,imgData[0].height, 0, GL_BGR, GL_UNSIGNED_BYTE, imgData[0].data);
	glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_X, 0, GL_RGBA, imgData[1].width, imgData[1].height, 0, GL_BGR, GL_UNSIGNED_BYTE, imgData[1].data);
	glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Y, 0, GL_RGBA, imgData[2].width, imgData[2].height, 0, GL_BGR, GL_UNSIGNED_BYTE, imgData[2].data);
	glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Y, 0, GL_RGBA, imgData[3].width, imgData[3].height, 0, GL_BGR, GL_UNSIGNED_BYTE, imgData[3].data);
	glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Z, 0, GL_RGBA, imgData[4].width, imgData[4].height, 0, GL_BGR, GL_UNSIGNED_BYTE, imgData[4].data);
	glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Z, 0, GL_RGBA, imgData[5].width, imgData[5].height, 0, GL_BGR, GL_UNSIGNED_BYTE, imgData[5].data);
	
	textureUniformLocation = glGetUniformLocation(Globals::SkyboxProgramId,"cubemap");
	
	cube.modelMatrix = mat4(1.0f);
	cube.MVP = Globals::Projection * Globals::View * cube.modelMatrix;	

	glGenBuffers(1,&vertexbuffer);
	glBindBuffer(GL_ARRAY_BUFFER,vertexbuffer);
	glBufferData(GL_ARRAY_BUFFER,sizeof(vertices),vertices,GL_STATIC_DRAW);

	glGenBuffers(1,&indexbuffer);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,indexbuffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER,sizeof(indices),indices,GL_STATIC_DRAW);
}

void unitCube::setup(const char* texturefile){
			
	cube.modelMatrix = mat4(1.0f);
	cube.MVP = Globals::Projection * Globals::View * cube.modelMatrix;	

	texture = loadBMP_custom(texturefile);

	textureUniformLocation = glGetUniformLocation(Globals::ProgramId,"texturesampler");

	glGenBuffers(1,&vertexbuffer);
	glBindBuffer(GL_ARRAY_BUFFER,vertexbuffer);
	glBufferData(GL_ARRAY_BUFFER,sizeof(vertices),vertices,GL_STATIC_DRAW);

	glGenBuffers(1,&normalbuffer);
	glBindBuffer(GL_ARRAY_BUFFER,normalbuffer);
	glBufferData(GL_ARRAY_BUFFER,sizeof(normals),normals,GL_STATIC_DRAW);

	glGenBuffers(1,&indexbuffer);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,indexbuffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER,sizeof(indices),indices,GL_STATIC_DRAW);

	glGenBuffers(1,&bindexbuffer);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,bindexbuffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER,sizeof(backindices),backindices,GL_STATIC_DRAW);
	
	glGenBuffers(1,&uvbuffer);
	glBindBuffer(GL_ARRAY_BUFFER,uvbuffer);
	glBufferData(GL_ARRAY_BUFFER,sizeof(texels),texels,GL_STATIC_DRAW);
		
}

void unitCylinder::setup(const char* texturefile){
	createBFaceBuffer();
	createTFaceBuffer();	
	createBodyBuffer();
		
	texture = loadBMP_custom(texturefile);
	textureUniformLocation = glGetUniformLocation(Globals::ProgramId,"texturesampler");

	glGenBuffers(1,&bvertexbufferId);
	glBindBuffer(GL_ARRAY_BUFFER,bvertexbufferId);
	glBufferData(GL_ARRAY_BUFFER,bvertexbuffer.size()*sizeof(GLfloat),&bvertexbuffer[0],GL_STATIC_DRAW);
	
	glGenBuffers(1,&tvertexbufferId);
	glBindBuffer(GL_ARRAY_BUFFER,tvertexbufferId);
	glBufferData(GL_ARRAY_BUFFER,tvertexbuffer.size()*sizeof(GLfloat),&tvertexbuffer[0],GL_STATIC_DRAW);
	
	glGenBuffers(1,&bodyvertexbufferId);
	glBindBuffer(GL_ARRAY_BUFFER,bodyvertexbufferId);
	glBufferData(GL_ARRAY_BUFFER,bodyvertexbuffer.size()*sizeof(GLfloat),&bodyvertexbuffer[0],GL_STATIC_DRAW);
	
	glGenBuffers(1,&faceuvbufferId);
	glBindBuffer(GL_ARRAY_BUFFER,faceuvbufferId);
	glBufferData(GL_ARRAY_BUFFER,faceuvbuffer.size()*sizeof(GLfloat),&faceuvbuffer[0],GL_STATIC_DRAW);
	
	glGenBuffers(1,&bodyuvbufferId);
	glBindBuffer(GL_ARRAY_BUFFER,bodyuvbufferId);
	glBufferData(GL_ARRAY_BUFFER,bodyuvbuffer.size()*sizeof(GLfloat),&bodyuvbuffer[0],GL_STATIC_DRAW);
}

void cuboid::createCuboid(float b, float l,float d,vec3 position,const char* texturefile){
	length = l;
	breadth = b;
	depth = d;
	unit.setup(texturefile);

	modelMatrix = mat4(1.0f);
	modelMatrix = translate(modelMatrix,vec3(position.x,position.y,position.z));
	modelMatrix = scale(modelMatrix,vec3(b,l,d));

	MVP = Globals::Projection * Globals::View * modelMatrix;
}

void cuboid::renderCuboid(){
	MVP = Globals::Projection * Globals::View * modelMatrix;
	glUniformMatrix4fv(Globals::MatrixId,1,GL_FALSE,&MVP[0][0]);
	glUniformMatrix4fv(Globals::model_mat_id,1,GL_FALSE,&modelMatrix[0][0]);
	glUniformMatrix4fv(Globals::view_mat_id, 1, GL_FALSE, &(Globals::View[0][0]));

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D,unit.texture);
	glUniform1i(unit.textureUniformLocation,0);

	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER,unit.vertexbuffer);
	glVertexAttribPointer(
		0,                  // attribute 0. No particular reason for 0, but must match the layout in the shader.
		3,                  // size
		GL_FLOAT,           // type
		GL_FALSE,           // normalized?
		0,                  // stride
		(void*)0            // array buffer offset
	);

	glEnableVertexAttribArray(1);
	glBindBuffer(GL_ARRAY_BUFFER,unit.uvbuffer);
	glVertexAttribPointer(
		1,                  // attribute 0. No particular reason for 0, but must match the layout in the shader.
		2,                  // size
		GL_FLOAT,           // type
		GL_FALSE,           // normalized?
		0,                  // stride
		(void*)0            // array buffer offset
	);

	glEnableVertexAttribArray(2);
	glBindBuffer(GL_ARRAY_BUFFER,unit.normalbuffer);
	glVertexAttribPointer(
		2,                  // attribute 0. No particular reason for 0, but must match the layout in the shader.
		3,                  // size
		GL_FLOAT,           // type
		GL_FALSE,           // normalized?
		0,                  // stride
		(void*)0            // array buffer offset
	);


	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,unit.indexbuffer);	
	glDrawElements(GL_TRIANGLES,sizeof(unit.indices)/sizeof(GLushort),GL_UNSIGNED_SHORT,0);
	glDisableVertexAttribArray(0);
	glDisableVertexAttribArray(1);
	glDisableVertexAttribArray(2);
}

/********************************************************************************************************/


void cuboid::createSkyboxCuboid(float b, float l,float d,vector<imageData> imgData){
	length = l;
	breadth = b;
	depth = d;
	unit.setupCubeMap(imgData);

	modelMatrix = mat4(1.0f);
	modelMatrix = translate(modelMatrix,vec3(0,0,0));
	modelMatrix = scale(modelMatrix,vec3(b,-l,d));

	MVP = Globals::Projection * Globals::View * modelMatrix;
}

void cuboid::renderSkyboxCuboid(){
	glDepthMask(GL_FALSE);
	MVP = Globals::Projection * mat4(mat3(Globals::View)) * modelMatrix;
	glUniformMatrix4fv(Globals::SkyboxMatrixId,1,GL_FALSE,&MVP[0][0]);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_CUBE_MAP,unit.texture);
	glUniform1i(unit.textureUniformLocation,0);

	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER,unit.vertexbuffer);
	glVertexAttribPointer(
		0,                  // attribute 0. No particular reason for 0, but must match the layout in the shader.
		3,                  // size
		GL_FLOAT,           // type
		GL_FALSE,           // normalized?
		0,                  // stride
		(void*)0            // array buffer offset
	);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,unit.indexbuffer);	
	glDrawElements(GL_TRIANGLES,sizeof(unit.indices)/sizeof(GLushort),GL_UNSIGNED_SHORT,0);
	glDisableVertexAttribArray(0);
	glDepthMask(GL_TRUE);
}

void cylinder::createCylinder(float r, float h, vec3 p, const char* texturefile){
	radius = r;
	height = h;
	position = p;

	unit.setup(texturefile);
	
	modelMatrix = mat4(1.0f);
	modelMatrix = translate(modelMatrix,vec3(position.x,position.y,position.z));
	modelMatrix = scale(modelMatrix,vec3(r,r,h));

	MVP = Globals::Projection * Globals::View * modelMatrix;
}

void cylinder::renderCylinder(){
	MVP = Globals::Projection * Globals::View * modelMatrix;
	glUniformMatrix4fv(Globals::MatrixId,1,GL_FALSE,&MVP[0][0]);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D,unit.texture);
	glUniform1i(unit.textureUniformLocation,0);

	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER,unit.bvertexbufferId);
	glVertexAttribPointer(
		0,                  // attribute 0. No particular reason for 0, but must match the layout in the shader.
		3,                  // size
		GL_FLOAT,           // type
		GL_FALSE,           // normalized?
		0,                  // stride
		(void*)0            // array buffer offset
	);

	glEnableVertexAttribArray(1);
	glBindBuffer(GL_ARRAY_BUFFER,unit.faceuvbufferId);
	glVertexAttribPointer(
		1,                  // attribute 0. No particular reason for 0, but must match the layout in the shader.
		2,                  // size
		GL_FLOAT,           // type
		GL_FALSE,           // normalized?
		0,                  // stride
		(void*)0            // array buffer offset
	);

	glDrawArrays(GL_TRIANGLES,0,unit.bvertexbuffer.size());
	glDisableVertexAttribArray(0);
	glDisableVertexAttribArray(1);

	
	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER,unit.bodyvertexbufferId);
	glVertexAttribPointer(
		0,                  // attribute 0. No particular reason for 0, but must match the layout in the shader.
		3,                  // size
		GL_FLOAT,           // type
		GL_FALSE,           // normalized?
		0,                  // stride
		(void*)0            // array buffer offset
	);

	glEnableVertexAttribArray(1);
	glBindBuffer(GL_ARRAY_BUFFER,unit.bodyuvbufferId);
	glVertexAttribPointer(
		1,                  // attribute 0. No particular reason for 0, but must match the layout in the shader.
		2,                  // size
		GL_FLOAT,           // type
		GL_FALSE,           // normalized?
		0,                  // stride
		(void*)0            // array buffer offset
	);

	glDrawArrays(GL_TRIANGLES,0,unit.bodyvertexbuffer.size());
	glDisableVertexAttribArray(0);
	glDisableVertexAttribArray(1);

	
	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER,unit.tvertexbufferId);
	glVertexAttribPointer(
		0,                  // attribute 0. No particular reason for 0, but must match the layout in the shader.
		3,                  // size
		GL_FLOAT,           // type
		GL_FALSE,           // normalized?
		0,                  // stride
		(void*)0            // array buffer offset
	);

	glEnableVertexAttribArray(1);
	glBindBuffer(GL_ARRAY_BUFFER,unit.faceuvbufferId);
	glVertexAttribPointer(
		1,                  // attribute 0. No particular reason for 0, but must match the layout in the shader.
		2,                  // size
		GL_FLOAT,           // type
		GL_FALSE,           // normalized?
		0,                  // stride
		(void*)0            // array buffer offset
	);


	glDrawArrays(GL_TRIANGLES,0,unit.tvertexbuffer.size());
	glDisableVertexAttribArray(0);
	glDisableVertexAttribArray(1);
}
