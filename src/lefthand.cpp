#include "../includes/parts.h"

void LeftHand::setup(SinglePivot p, const char *texture) {
	parent = p;
	parentMatrix = parent.getModelMatrix();
	vec3 parentPos = vec3(parentMatrix[3]);
		
	lefthand.createCuboid(width,height,depth,parentPos+offsetFromParent,texture);
	position = vec3(lefthand.modelMatrix[3]);
	components.push_back(lefthand);			

}

void LeftHand::render() {
	commonRender();
}

void LeftHand::readjust(mat4 parentMatrix){
	mat4 newModel = translate(parentMatrix,offsetFromParent);
	newModel = scale(newModel,vec3(width,height,depth));
	components[0].modelMatrix = newModel;
}
