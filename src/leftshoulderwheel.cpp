#include "../includes/parts.h"

void LeftShoulderWheel::setup(SinglePivot p, const char *texture){
	parent = p;
	parentMatrix = parent.getModelMatrix();
	vec3 parentPos = vec3(parentMatrix[3]);
	
	leftshoulderwheel.createCylinder(radius,height,parentPos+offsetFromParent,texture);
	position = vec3(leftshoulderwheel.modelMatrix[3]);

	cylcomponents.push_back(leftshoulderwheel);	
}

void LeftShoulderWheel::render(){
	commonCylinderRender();
}

void LeftShoulderWheel::readjust(mat4 parentMatrix){
	mat4 newModel = translate(parentMatrix,offsetFromParent);
	newModel = scale(newModel,vec3(radius,radius,height));
	cylcomponents[0].modelMatrix = newModel;
}
