#include "../includes/transformer.h"

void RightThigh::setup(SinglePivot p, const char *texture) {
	parent = p;
	parentMatrix = parent.getModelMatrix();
	vec3 parentPos = vec3(parentMatrix[3]);

	rightthigh.createCuboid(width,height,depth,parentPos+offsetFromParent,texture);
	position = vec3(rightthigh.modelMatrix[3]);
	components.push_back(rightthigh);						
	
	rightLegPivot.setup(position,vec3(0,-5.5,0),pivottexture);
}

void RightThigh::render() {
	commonRender();
	rightLegPivot.render();	
}

void RightThigh::readjust(mat4 parentMatrix, Transformer &t){
	mat4 newModel = translate(parentMatrix,offsetFromParent);
	newModel = scale(newModel,vec3(width,height,depth));
	components[0].modelMatrix = newModel;

	mat4 unscale = scale(components[0].modelMatrix,vec3(1.0f/width,1.0f/height,1.0f/depth));
	t.bumbleRightThigh.rightLegPivot.readjustRightLegPivot(unscale,t);
}
