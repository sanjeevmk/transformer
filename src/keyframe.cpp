#include"../includes/csv.h"
#include"../includes/keyframe.hpp"
#include"../includes/transformer.h"
#include"../includes/csv.h"
#include<vector>
#include<iostream>

using namespace std;

extern vector<Transformer*> transformersList;

void captureKeyFrame(){
	keyframe k;	

	float time;

	printf("Enter timestamp\n");
	scanf("%f",&time);

	k.timestamp = time;

	vector<Transformer*>::iterator transiterate = transformersList.begin();
	while(transiterate!=transformersList.end()){
		map<string,float> prop;

		vec3 pos = vec3((**transiterate).bumbleTorso.components[0].modelMatrix[3]);	
		prop["px"] = pos.x;
		prop["py"] = pos.y;
		prop["pz"] = pos.z;

		vec3 rot = (**transiterate).bumbleTorso.neckPivot.getRotationAngle();
		prop["nx"] = rot.x;
		prop["ny"] = rot.y;
		prop["nz"] = rot.z;
		
		rot = (**transiterate).bumbleTorso.leftShoulderPivot.getRotationAngle();
		prop["lsx"] = rot.x;
		prop["lsy"] = rot.y;
		prop["lsz"] = rot.z;
		
		rot = (**transiterate).bumbleTorso.rightShoulderPivot.getRotationAngle();
		prop["rsx"] = rot.x;
		prop["rsy"] = rot.y;
		prop["rsz"] = rot.z;
		
		rot = (**transiterate).bumbleTorso.leftThighPivot.getRotationAngle();
		prop["ltx"] = rot.x;
		prop["lty"] = rot.y;
		prop["ltz"] = rot.z;
		
		rot = (**transiterate).bumbleTorso.leftThighTPivot.getRotationAngle();
		prop["lttx"] = rot.x;
		prop["ltty"] = rot.y;
		prop["lttz"] = rot.z;
		
		rot = (**transiterate).bumbleTorso.rightThighPivot.getRotationAngle();
		prop["rtx"] = rot.x;
		prop["rty"] = rot.y;
		prop["rtz"] = rot.z;
		
		rot = (**transiterate).bumbleLeftShoulder.leftArmPivot.getRotationAngle();
		prop["lax"] = rot.x;
		prop["lay"] = rot.y;
		prop["laz"] = rot.z;
		
		rot = (**transiterate).bumbleLeftShoulder.leftShoulderWheelPivot.getRotationAngle();
		prop["lswx"] = rot.x;
		prop["lswy"] = rot.y;
		prop["lswz"] = rot.z;

		rot = (**transiterate).bumbleRightShoulder.rightArmPivot.getRotationAngle();
		prop["rax"] = rot.x;
		prop["ray"] = rot.y;
		prop["raz"] = rot.z;
		
		rot = (**transiterate).bumbleRightShoulder.rightShoulderWheelPivot.getRotationAngle();
		prop["rswx"] = rot.x;
		prop["rswy"] = rot.y;
		prop["rswz"] = rot.z;
		
		rot = (**transiterate).bumbleLeftArm.leftWristPivot.getRotationAngle();
		prop["lwx"] = rot.x;
		prop["lwy"] = rot.y;
		prop["lwz"] = rot.z;
		
		rot = (**transiterate).bumbleLeftArm.leftArmWheelPivot.getRotationAngle();
		prop["lawx"] = rot.x;
		prop["lawy"] = rot.y;
		prop["lawz"] = rot.z;
		
		rot = (**transiterate).bumbleRightArm.rightWristPivot.getRotationAngle();
		prop["rwx"] = rot.x;
		prop["rwy"] = rot.y;
		prop["rwz"] = rot.z;
		
		rot = (**transiterate).bumbleRightArm.rightArmWheelPivot.getRotationAngle();
		prop["rawx"] = rot.x;
		prop["rawy"] = rot.y;
		prop["rawz"] = rot.z;
		
		rot = (**transiterate).bumbleLeftThigh.leftLegPivot.getRotationAngle();
		prop["llx"] = rot.x;
		prop["lly"] = rot.y;
		prop["llz"] = rot.z;
		
		rot = (**transiterate).bumbleRightThigh.rightLegPivot.getRotationAngle();
		prop["rlx"] = rot.x;
		prop["rly"] = rot.y;
		prop["rlz"] = rot.z;
		
		rot = (**transiterate).bumbleLeftLeg.leftFootPivot.getRotationAngle();
		prop["lfx"] = rot.x;
		prop["lfy"] = rot.y;
		prop["lfz"] = rot.z;
		
		rot = (**transiterate).bumbleRightLeg.rightFootPivot.getRotationAngle();
		prop["rfx"] = rot.x;
		prop["rfy"] = rot.y;
		prop["rfz"] = rot.z;

		prop["turn"] = (**transiterate).turnAngle;
		k.transformerProp.push_back(prop);
		transiterate++;
	}

	k.globalProp["mode"] = Globals::mode;
	k.globalProp["headl"] = 0;
	k.globalProp["streetl"] = 1;
	k.globalProp["dirpx"] = Globals::dir_camera_position.x;
	k.globalProp["dirpy"] = Globals::dir_camera_position.y;
	k.globalProp["dirpz"] = Globals::dir_camera_position.z;
	
	k.globalProp["dirrx"] = Globals::dirX;
	k.globalProp["dirry"] = Globals::dirY;
	k.globalProp["dirrz"] = Globals::dirZ;

	k.globalProp["active"] = Globals::activeIndex;
	k.globalProp["cammode"] = Globals::currentMode;

	keyframes.push_back(k);
	keyframes.push_back(k);
}

void pushKeyFrames(){
	cout << keyframes.size() << endl;
	writeCsv(keyframes,2,"keyframes.txt");
}
