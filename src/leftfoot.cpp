#include "../includes/parts.h"

void LeftFoot::setup(SinglePivot p, const char *texture) {
	parent = p;
	parentMatrix = parent.getModelMatrix();
	vec3 parentPos = vec3(parentMatrix[3]);
	
	leftfoot.createCuboid(width,height,depth,parentPos+offsetFromParent,texture);
	position = vec3(leftfoot.modelMatrix[3]);
	components.push_back(leftfoot);
}

void LeftFoot::render() {
	commonRender();
}

void LeftFoot::readjust(mat4 parentMatrix){
	mat4 newModel = translate(parentMatrix,offsetFromParent);
	newModel = scale(newModel,vec3(width,height,depth));
	components[0].modelMatrix = newModel;
}
