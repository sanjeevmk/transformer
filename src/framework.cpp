#include <stdio.h>
#include "../includes/globals.h"
#include "../includes/framework.hpp"
#include "../common/shader.hpp"
#define GLM_FORCE_RADIANS
#include "../glm/glm/glm.hpp"
#include "../glm/glm/gtc/matrix_transform.hpp"
#include "../includes/transformer.h"

using namespace glm;

GLuint Globals::MatrixId;
GLuint Globals::SkyboxMatrixId;
GLuint Globals::ProgramId;
GLuint Globals::SkyboxProgramId;
GLuint Globals::view_mat_id, Globals::model_mat_id, Globals::proj_mat_id;
mat4 Globals::Projection, Globals::View, Globals::fpView, Globals::chaseView, Globals::globalView, Globals::TransformerMatrix, Globals::directorView;
float Globals::angle, Globals::dirX,Globals::dirY,Globals::dirZ;
vec3 Globals::fpOffset, Globals::chaseOffset, Globals::carPosition, Globals::fpLookAt, Globals::head1off, Globals::head2off, Globals::dir_camera_position, Globals::dir_look;
int Globals::currentMode;
int Globals::mode, Globals::streetlights, Globals::headlights, Globals::activeIndex;

Transformer *Globals::activeTransformer;

void globalSetup(){
	Globals::ProgramId = LoadShaders("CubeVertexShader.vertexshader","CubeFragmentShader.fragmentshader");
	Globals::SkyboxProgramId = LoadShaders("SkyboxVertex.vertexshader","SkyboxFragment.fragmentshader");

	Globals::MatrixId = glGetUniformLocation(Globals::ProgramId,"MVP");
	Globals::SkyboxMatrixId = glGetUniformLocation(Globals::SkyboxProgramId,"PVM");

	Globals::view_mat_id = glGetUniformLocation(Globals::ProgramId,"view_mat");
	assert(Globals::view_mat_id > 1);
	Globals::model_mat_id = glGetUniformLocation(Globals::ProgramId,"model_mat");

	Globals::Projection = perspective(45.0f,1024.0f/768.0f,0.1f,2000.0f);
	Globals::View = lookAt(
				vec3(0,50,-900),
				vec3(0,50,900),
				vec3(0,1,0)
			);

	Globals::head1off = vec3(0,0,15);
	Globals::head2off = vec3(0,0,15);

	Globals::set_global_camera(vec3(900,200,0),vec3(-900,200,0), vec3(0,50,80), vec3(0,0,0));
	Globals::currentMode = 0;

	Globals::mode = 0;
	Globals::streetlights = 1;
	Globals::headlights = 0;

	glUniformMatrix4fv(Globals::proj_mat_id, 1, GL_FALSE, &(Globals::Projection[0][0]));
	glEnable(GL_TEXTURE_CUBE_MAP_SEAMLESS);
}
