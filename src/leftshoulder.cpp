#include "../includes/transformer.h"

void LeftShoulder::setup(SinglePivot p, const char *texture) {
	parent = p;
	parentMatrix = parent.getModelMatrix();
	vec3 parentPos = vec3(parentMatrix[3]);

	leftshoulder.createCuboid(width,height,depth,parentPos+offsetFromParent,texture);
	position = vec3(leftshoulder.modelMatrix[3]);

	components.push_back(leftshoulder);						
	
	leftArmPivot.setup(position,vec3(0,-3.5,0),pivottexture);
	leftShoulderWheelPivot.setup(position,vec3(0,0,-2.0),pivottexture);
}

void LeftShoulder::render() {
	commonRender();
	leftArmPivot.render();	
	leftShoulderWheelPivot.render();	
}

void LeftShoulder::readjust(mat4 parentMatrix, Transformer &t){
	mat4 newModel = translate(parentMatrix,offsetFromParent);
	newModel = scale(newModel,vec3(width,height,depth));
	components[0].modelMatrix = newModel;

	mat4 unscale = scale(components[0].modelMatrix,vec3(1.0f/width,1.0f/height,1.0f/depth));
	t.bumbleLeftShoulder.leftArmPivot.readjustLeftArmPivot(unscale,t);
	t.bumbleLeftShoulder.leftShoulderWheelPivot.readjustLeftShoulderWheelPivot(unscale,t);
}
