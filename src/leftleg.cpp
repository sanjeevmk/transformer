#include "../includes/transformer.h"

void LeftLeg::setup(SinglePivot p, const char *texture) {
	parent = p;
	parentMatrix = parent.getModelMatrix();
	vec3 parentPos = vec3(parentMatrix[3]);

	leftleg.createCuboid(width,height,depth,parentPos+offsetFromParent,texture);
	position = vec3(leftleg.modelMatrix[3]);
	components.push_back(leftleg);						
	
	leftFootPivot.setup(position,vec3(0,-5.5,0),pivottexture);
}

void LeftLeg::render() {
	commonRender();
	leftFootPivot.render();	
}

void LeftLeg::readjust(mat4 parentMatrix, Transformer &t){
	mat4 newModel = translate(parentMatrix,offsetFromParent);
	newModel = scale(newModel,vec3(width,height,depth));
	components[0].modelMatrix = newModel;

	mat4 unscale = scale(components[0].modelMatrix,vec3(1.0f/width,1.0f/height,1.0f/depth));
	t.bumbleLeftLeg.leftFootPivot.readjustLeftFootPivot(unscale,t);
}
