#include "../includes/parts.h"

void RightArmWheel::setup(SinglePivot p, const char *texture){
	parent = p;
	parentMatrix = parent.getModelMatrix();
	vec3 parentPos = vec3(parentMatrix[3]);
	
	rightarmwheel.createCylinder(radius,height,parentPos+offsetFromParent,texture);
	position = vec3(rightarmwheel.modelMatrix[3]);

	cylcomponents.push_back(rightarmwheel);	
}

void RightArmWheel::render(){
	commonCylinderRender();
}

void RightArmWheel::readjust(mat4 parentMatrix){
	mat4 newModel = translate(parentMatrix,offsetFromParent);
	newModel = scale(newModel,vec3(radius,radius,height));
	cylcomponents[0].modelMatrix = newModel;
}
