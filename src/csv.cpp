#include <vector>
#include <set>
#include <map>
#include <algorithm>
#include <string>
#include <fstream>
#include <iostream>
#include <cstdio>
#include <iterator>
#include "../includes/csv.h"
using namespace std;

void print(vector<keyframe>& keyframes){
	cout << "Number of keyframes = " <<nKeyframes << endl;
	cout << "Number of transformers = " <<nTransformers << endl;
	for(int i=0;i<nKeyframes; ++i){
		cout << "Timestamp : " << keyframes[i].timestamp;
		for(int j=0; j<nTransformers+1; ++j){
			map<string,float>::iterator it;
			for(it = keyframes[i].transformerProp[j].begin(); it!=keyframes[i].transformerProp[j].end(); it++){
				cout << " Transformer : " << j << " " << it->first << " = " << it->second << "\t";
			}
		}
		map<string,float>::iterator it;
		for(it = keyframes[i].globalProp.begin(); it!=keyframes[i].globalProp.end(); it++){
			cout  << " Global : " << it->first << " = " << it->second << "\t";
		}
		cout << endl;
	}
}

vector<keyframe> readCsv(string filename){
	/*
	 *In file, first line will be number of transformers.
	 *After first line, actual data will come
	 *First field in each dataline will be timestamp
	 *It is assumed that each dataline has atleast three comma separated values: 
	 */
	ifstream file(filename.c_str());
	if(file.is_open()){
		string line;
		vector<string> lines;

		string firstLine;
		getline(file, firstLine);
		int max = -1;
		while(getline(file, line)){
			lines.push_back(line);
		}

		nKeyframes = lines.size();
		nTransformers = atoi(firstLine.c_str());
		keyframes.resize(nKeyframes);
		for(int i=0; i<nKeyframes; ++i)
			keyframes[i].transformerProp.resize(nTransformers);
		//Parsing
		for(int i=0; i<lines.size(); ++i){
			string str = lines[i];
			if(str.find(',') == string::npos){
				cout << "Format of " << filename << " is not proper. Exiting now." << endl;
			}

			float tempTimestamp = atof(str.substr(0, str.find(',')).c_str());
			keyframes[i].timestamp = tempTimestamp;
			str = str.substr(str.find(',') + 1, str.size() - 1);
			str += ", "; 		
			while(str.find(',') != string::npos){
				string attr = str.substr(0, str.find(','));
				str = str.substr(str.find(',') + 1, str.size() - 1);
				string value = str.substr(0, str.find(','));
				str = str.substr(str.find(',') + 1, str.size() - 1);
				if(attr.find("transformer") != string::npos){
					attr = attr.substr(attr.find("transformer") + 11, attr.size()-1);
					int idx = atoi(attr.substr(0,2).c_str());
					attr = attr.substr(2, attr.size()-1);
					keyframes[i].transformerProp[idx][attr] = atof(value.c_str());
				}else{
					keyframes[i].globalProp[attr] = atof(value.c_str());
				}
			}

		}

	}
	file.close();

	return keyframes;
}

void writeCsv(vector<keyframe>& keyframes,int nTransformers, string filename){
	ofstream file(filename.c_str());
	string str = "";
	if(file.is_open()){
		str += to_string(nTransformers);
		str += '\n';
		for(int k=0;k<keyframes.size(); k+=2){
			str += to_string(keyframes[k].timestamp);
			for(int i=0;i<keyframes[k].transformerProp.size();++i){
				map<string,float>::iterator it;
				for(it=keyframes[k].transformerProp[i].begin();it!=keyframes[k].transformerProp[i].end();it++){
					str += ',';
					str += "transformer";
					if(i < 9)
						str += to_string(0);
					str += to_string(i);
					str += it->first;
					str += ',';
					str += to_string(it->second);

				}
			}

			map<string,float>::iterator it1;
			for(it1=keyframes[k].globalProp.begin();it1!=keyframes[k].globalProp.end();it1++){
				str += ',';
				str += it1->first;
				str += ',';
				str += to_string(it1->second);
			}
			str += '\n';

		}
		file.write(str.c_str(),str.size());
		file.flush();
		file.close();
	}
}


/*
//For testing
int main(){
readCsv("sample.csv");
writeCsv(keyframes,5, "sample2.csv");
readCsv("sample2.csv");
cout << "Now " << endl;
print(keyframes);
return 0;
}
 */
