#include "../includes/parts.h"

void RightShoulderWheel::setup(SinglePivot p, const char *texture){
	parent = p;
	parentMatrix = parent.getModelMatrix();
	vec3 parentPos = vec3(parentMatrix[3]);
	
	rightshoulderwheel.createCylinder(radius,height,parentPos+offsetFromParent,texture);
	position = vec3(rightshoulderwheel.modelMatrix[3]);

	cylcomponents.push_back(rightshoulderwheel);	
}

void RightShoulderWheel::render(){
	commonCylinderRender();
}

void RightShoulderWheel::readjust(mat4 parentMatrix){
	mat4 newModel = translate(parentMatrix,offsetFromParent);
	newModel = scale(newModel,vec3(radius,radius,height));
	cylcomponents[0].modelMatrix = newModel;
}
