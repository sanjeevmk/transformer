#include "../includes/transformer.h"

void LeftArm::setup(SinglePivot p, const char* texture) {
	parent = p;
	parentMatrix = parent.getModelMatrix();
	vec3 parentPos = vec3(parentMatrix[3]);
	
	leftarm.createCuboid(width,height,depth,parentPos+offsetFromParent,texture);
	position = vec3(leftarm.modelMatrix[3]);

	components.push_back(leftarm);	

	leftWristPivot.setup(position,vec3(0,-5.5,0),pivottexture);
	leftArmWheelPivot.setup(position,vec3(0,-2.5,-2.0),pivottexture);
}

void LeftArm::render() {
	commonRender();
	leftWristPivot.render();
	leftArmWheelPivot.render();
}

void LeftArm::readjust(mat4 parentMatrix, Transformer &t){
	mat4 newModel = translate(parentMatrix,offsetFromParent);
	newModel = scale(newModel,vec3(width,height,depth));
	components[0].modelMatrix = newModel;
	
	mat4 unscale = scale(components[0].modelMatrix,vec3(1.0f/width,1.0f/height,1.0f/depth));
	t.bumbleLeftArm.leftWristPivot.readjustLeftWristPivot(unscale,t);
	t.bumbleLeftArm.leftArmWheelPivot.readjustLeftArmWheelPivot(unscale,t);
}
