#include "../includes/parts.h"
#include "../glm/glm/gtx/matrix_interpolation.hpp"
#include <cmath>
using namespace glm;

void BodyPart::commonRender(){
	vector<cuboid>::iterator compoiterate = components.begin();
	
	while(compoiterate != components.end()){
		(*compoiterate).renderCuboid();
		compoiterate++;
	}
}

void BodyPart::commonCylinderRender(){
	vector<cylinder>::iterator compoiterate = cylcomponents.begin();
	
	while(compoiterate != cylcomponents.end()){
		(*compoiterate).renderCylinder();
		compoiterate++;
	}
}

void SinglePivotBase::setup(vec3 parentPos,vec3 offset,const char* texturef) {
	pivot.createCuboid(1.0f,1.0f,1.0f,parentPos+offset,texturef);
	offsetFromParent = offset;
}

void SinglePivotBase::render(){
	pivot.renderCuboid();
}

mat4 SinglePivotBase::getModelMatrix(){
	return pivot.modelMatrix;
}

vec3 SinglePivotBase::getRotationAngle(){
	return vec3(rotX,rotY,rotZ);
}

vec3 SinglePivotBase::getRotationAnglet(){
	return vec3(rotXt,rotYt,rotZt);
}

void SinglePivotBase::setModelMatrix(mat4 givenMatrix){
	pivot.modelMatrix = givenMatrix;
}
