#include "../includes/transformer.h"

void RightShoulder::setup(SinglePivot p, const char *texture) {
	parent = p;
	parentMatrix = parent.getModelMatrix();
	vec3 parentPos = vec3(parentMatrix[3]);
	
	rightshoulder.createCuboid(width,height,depth,parentPos+offsetFromParent,texture);
	position = vec3(rightshoulder.modelMatrix[3]);

	components.push_back(rightshoulder);						
	
	rightArmPivot.setup(position,vec3(0,-3.5,0),pivottexture);
	rightShoulderWheelPivot.setup(position,vec3(0,0,-2.0),pivottexture);
}

void RightShoulder::render() {
	commonRender();
	rightArmPivot.render();	
	rightShoulderWheelPivot.render();	
}

void RightShoulder::readjust(mat4 parentMatrix, Transformer &t){
	mat4 newModel = translate(parentMatrix,offsetFromParent);
	newModel = scale(newModel,vec3(width,height,depth));
	components[0].modelMatrix = newModel;
	
	mat4 unscale = scale(components[0].modelMatrix,vec3(1.0f/width,1.0f/height,1.0f/depth));
	t.bumbleRightShoulder.rightArmPivot.readjustRightArmPivot(unscale,t);
	t.bumbleRightShoulder.rightShoulderWheelPivot.readjustRightShoulderWheelPivot(unscale,t);
}
