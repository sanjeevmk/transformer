#include "../includes/transformer.h"

void Torso::setup(vec3 position,const char *bumbletexture) {

	torso.createCuboid(width,height,depth,position,bumbletexture);
	components.push_back(torso);
	
	neckPivot.setup(position,vec3(0,height+1,0),pivottexture);
	
	leftShoulderPivot.setup(position,vec3(-width-1.0,height-2,0),pivottexture);
	
	rightShoulderPivot.setup(position,vec3( width+1.0,height-2.0,0),pivottexture);
	
	leftThighPivot.setup(position,vec3(-(width/2.0f),-height-1.0,0),pivottexture);
	leftThighTPivot.setup(position,vec3(-(width/2.0f),-height-1.0,0),pivottexture);

	rightThighPivot.setup(position,vec3((width/2.0f),-height-1.0,0),pivottexture);
}

void Torso::render() {
	Globals::TransformerMatrix = (*Globals::activeTransformer).bumbleTorso.components[0].modelMatrix;
	commonRender();
	neckPivot.render();
	leftShoulderPivot.render();
	rightShoulderPivot.render();
	leftThighPivot.render();
	rightThighPivot.render();
}

void Torso::readjust(mat4 parentMatrix, Transformer &t){
	mat4 newModel = glm::translate(parentMatrix,offsetFromParent);
	newModel = scale(newModel,vec3(width,height,depth));
	components[0].modelMatrix = newModel;

	mat4 unscale = scale(components[0].modelMatrix,vec3(1.0f/width,1.0f/height,1.0f/depth));
	t.bumbleTorso.neckPivot.readjustNeckPivot(unscale,t);
	t.bumbleTorso.leftShoulderPivot.readjustLeftShoulderPivot(unscale,t);
	t.bumbleTorso.rightShoulderPivot.readjustRightShoulderPivot(unscale,t);
}

void Torso::translate(vec3 trans, Transformer &t){
	if(Globals::mode!=1)
		return;

	printf("translate called\n");
	mat4 unscale = scale(components[0].modelMatrix,vec3(1.0f/width,1.0f/height,1.0f/depth));
	mat4 newModel = glm::translate(unscale,trans);
	newModel = scale(newModel,vec3(width,height,depth));
	components[0].modelMatrix = newModel;

	unscale = scale(components[0].modelMatrix,vec3(1.0f/width,1.0f/height,1.0f/depth));
	t.bumbleTorso.neckPivot.readjustNeckPivot(unscale,t);
	t.bumbleTorso.leftShoulderPivot.readjustLeftShoulderPivot(unscale,t);
	t.bumbleTorso.rightShoulderPivot.readjustRightShoulderPivot(unscale,t);
	t.bumbleTorso.leftThighPivot.readjustLeftThighPivot(unscale,t);
	t.bumbleTorso.rightThighPivot.readjustRightThighPivot(unscale,t);
}

void Torso::rotate(float angle, Transformer &t){
	vec3 axis = vec3(0,0,1);

	mat4 unscale = scale(components[0].modelMatrix,vec3(1.0f/width,1.0f/height,1.0f/depth));
	mat4 newModel = glm::rotate(unscale,angle,axis);
	newModel = scale(newModel,vec3(width,height,depth));
	components[0].modelMatrix = newModel;
	
	unscale = scale(components[0].modelMatrix,vec3(1.0f/width,1.0f/height,1.0f/depth));
	t.bumbleTorso.neckPivot.readjustNeckPivot(unscale,t);
	t.bumbleTorso.leftShoulderPivot.readjustLeftShoulderPivot(unscale,t);
	t.bumbleTorso.rightShoulderPivot.readjustRightShoulderPivot(unscale,t);
	t.bumbleTorso.leftThighPivot.readjustLeftThighPivot(unscale,t);
	t.bumbleTorso.rightThighPivot.readjustRightThighPivot(unscale,t);
	t.bumbleTorso.leftThighPivot.readjustLeftThighTPivot(unscale,t);

	t.turnAngle += angle;

	Globals::angle = (*Globals::activeTransformer).turnAngle;
}



