#include<stdio.h>
#include<vector>
#include<unistd.h>

using namespace std;
// Include GLM
#define GLM_FORCE_RADIANS
#include "../glm/glm/gtc/matrix_transform.hpp"
using namespace glm;

#include "../includes/globals.h"
#include "../includes/controls.hpp"
#include "../includes/transformer.h"
#include "../includes/transformer.hpp"
#include "../includes/scene.h"
#include "../includes/keyframe.hpp"

// Include GLFW
#include <GLFW/glfw3.h>
extern GLFWwindow* mainwindow;

#define PI 3.14159265359
#define toradians(x) (PI/180.0)*x

vector<vec3> positions {vec3(0,800,-400)};

bool turned = false;
bool pressed = false;
enum Parts{
	HEAD,
	LEFTHAND,
	RIGHTHAND,
	LEFTFOOT,
	RIGHTFOOT,
	LEFTARM,
	RIGHTARM,
	LEFTSHOULDER,
	RIGHTSHOULDER,
	LEFTLEG,
	RIGHTLEG,
	LEFTTHIGH,
	RIGHTTHIGH,
	TORSO,
	CAR,
	DIRCAM,
	DIRCAMR
};

extern vector<Transformer*> transformersList;

extern Skybox skyBox;
extern Torso bumbleTorso;
extern Head bumbleHead;
extern LeftShoulder bumbleLeftShoulder;
extern RightShoulder bumbleRightShoulder;
extern LeftArm bumbleLeftArm;
extern RightArm bumbleRightArm;
extern LeftHand bumbleLeftHand;
extern RightHand bumbleRightHand;
extern LeftThigh bumbleLeftThigh;
extern RightThigh bumbleRightThigh;
extern LeftLeg bumbleLeftLeg;
extern RightLeg bumbleRightLeg;
extern LeftFoot bumbleLeftFoot;
extern RightFoot bumbleRightFoot;

int flag = -1;
int dir = 1;
float rotangle = 0.0001f;

float speed = 300.0f; 
float speeda = 0.2f; 

void computeMatricesFromInputs(){

	static int i = 0;
	
	// glfwGetTime is called only once, the first time this function is called
	static double lastTime = glfwGetTime();

	// Compute time difference between current and last frame
	double currentTime = glfwGetTime();
	float deltaTime = float(currentTime - lastTime);
/*
	if (glfwGetKey( mainwindow, GLFW_KEY_S ) == GLFW_PRESS){
		sleep(1);
		Globals::currentMode = 3;
		Globals::View   = lookAt(
				positions[i],          
				vec3(0,0,0),
				vec3(0,1,0)          
		   );
		if(i<positions.size()-1)
			i++;
		else
			i=0;
	}
*/	
	if (glfwGetKey( mainwindow, GLFW_KEY_SEMICOLON ) == GLFW_PRESS){
		captureKeyFrame();
	}
	
	if (glfwGetKey( mainwindow, GLFW_KEY_SPACE ) == GLFW_PRESS){
		static int cnt=0;

		if(cnt==1)
			return;
		pushKeyFrames();
		cnt++;
	}
	
	if (glfwGetKey( mainwindow, GLFW_KEY_RIGHT_SHIFT ) == GLFW_PRESS){
		static int cnt = 0;

		if (!(*Globals::activeTransformer).car) {
			(*Globals::activeTransformer).bumbleTorso.leftThighTPivot.rotateLeftThighTPivot(toradians(90.0f),vec3(1,0,0),(*Globals::activeTransformer));
			(*Globals::activeTransformer).bumbleTorso.leftShoulderPivot.rotateLeftShoulderPivot(toradians(90.0f),vec3(0,1,0),(*Globals::activeTransformer));
			(*Globals::activeTransformer).bumbleTorso.rightShoulderPivot.rotateRightShoulderPivot(toradians(-90.0f),vec3(0,1,0),(*Globals::activeTransformer));
			(*Globals::activeTransformer).bumbleLeftArm.leftWristPivot.rotateLeftWristPivot(toradians(90.0f),vec3(0,0,1),(*Globals::activeTransformer));
			(*Globals::activeTransformer).bumbleRightArm.rightWristPivot.rotateRightWristPivot(toradians(-90.0f),vec3(0,0,1),(*Globals::activeTransformer));
			(*Globals::activeTransformer).bumbleTorso.leftThighPivot.rotateLeftThighPivot(toradians(90.0f),vec3(1,0,0),true,(*Globals::activeTransformer));
			(*Globals::activeTransformer).bumbleTorso.rightThighPivot.rotateRightThighPivot(toradians(90.0f),vec3(1,0,0),true,(*Globals::activeTransformer));
			(*Globals::activeTransformer).bumbleLeftThigh.leftLegPivot.rotateLeftLegPivot(toradians(-180.0f),vec3(1,0,0),(*Globals::activeTransformer));
			(*Globals::activeTransformer).bumbleRightThigh.rightLegPivot.rotateRightLegPivot(toradians(-180.0f),vec3(1,0,0),(*Globals::activeTransformer)); 
			Globals::mode = 1;
			(*Globals::activeTransformer).bumbleTorso.translate(vec3(0,0,16),(*Globals::activeTransformer));

			(*Globals::activeTransformer).car = true;
		}
		Globals::headlights = 1;
		flag = CAR;
		if (cnt == 0) {
			Globals::init_cameras();
			cnt++;
		}
	}
	
	if (glfwGetKey( mainwindow, GLFW_KEY_H ) == GLFW_PRESS){
		Globals::headlights = 1 - Globals::headlights;
	}
	
	if (glfwGetKey( mainwindow, GLFW_KEY_L ) == GLFW_PRESS){
		Globals::streetlights = 1 - Globals::streetlights;
	}
	
	if (glfwGetKey( mainwindow, GLFW_KEY_Q ) == GLFW_PRESS){
		flag = HEAD;
	}
	
	if (glfwGetKey( mainwindow, GLFW_KEY_W ) == GLFW_PRESS){
		flag = LEFTSHOULDER;
	}
	
	if (glfwGetKey( mainwindow, GLFW_KEY_E ) == GLFW_PRESS){
		flag = RIGHTSHOULDER;
	}
	
	if (glfwGetKey( mainwindow, GLFW_KEY_R ) == GLFW_PRESS){
		flag = LEFTARM;
	}
	
	if (glfwGetKey( mainwindow, GLFW_KEY_T ) == GLFW_PRESS){
		flag = RIGHTARM;
	}
	
	if (glfwGetKey( mainwindow, GLFW_KEY_U ) == GLFW_PRESS){
		flag = LEFTHAND;
	}
	
	if (glfwGetKey( mainwindow, GLFW_KEY_I ) == GLFW_PRESS){
		flag = RIGHTHAND;
	}
	
	if (glfwGetKey( mainwindow, GLFW_KEY_O ) == GLFW_PRESS){
		flag = LEFTTHIGH;
	}
	
	if (glfwGetKey( mainwindow, GLFW_KEY_P ) == GLFW_PRESS){
		flag = RIGHTTHIGH;
	}
	
	if (glfwGetKey( mainwindow, GLFW_KEY_A ) == GLFW_PRESS){
		flag = LEFTLEG;
	}
	
	if (glfwGetKey( mainwindow, GLFW_KEY_S ) == GLFW_PRESS){
		static int cnt = 0;
		if(cnt==1)
			return;
		Globals::mode = 1;
		for(int index=0; index<transformersList.size(); index++){
			(*transformersList[index]).bumbleTorso.translate(vec3(0,0,16),(*transformersList[index]));
		}
		(*Globals::activeTransformer).car = true;
		Globals::headlights = 1;
		Globals::streetlights = 1;
		flag = CAR;
		if (cnt == 0) {
			Globals::init_cameras();
		}
		cnt++;
//		flag = RIGHTLEG;
	}
	
	if (glfwGetKey( mainwindow, GLFW_KEY_D ) == GLFW_PRESS){
		//flag = LEFTFOOT;
		static int cnt = 0;
		if(cnt==1)
			return;
		for(int index=0; index<transformersList.size(); index++){
			(*transformersList[index]).bumbleLeftThigh.leftLegPivot.rotateLeftLegPivot(toradians(180.0f),vec3(1,0,0),*transformersList[index]);
			(*transformersList[index]).bumbleRightThigh.rightLegPivot.rotateRightLegPivot(toradians(180.0f),vec3(1,0,0),*transformersList[index]); 
		}
		cnt++;
	}
	
	if (glfwGetKey( mainwindow, GLFW_KEY_J ) == GLFW_PRESS){
//		flag = RIGHTFOOT;
		static int cnt = 0;
		if(cnt==1)
			return;
		for(int index=0; index<transformersList.size(); index++){
			(*transformersList[index]).bumbleTorso.leftThighPivot.rotateLeftThighPivot(toradians(90.0f),vec3(1,0,0),true,*transformersList[index]);
			(*transformersList[index]).bumbleTorso.rightThighPivot.rotateRightThighPivot(toradians(90.0f),vec3(1,0,0),true,*transformersList[index]);
		}
		cnt++;
	}
	
	if (glfwGetKey( mainwindow, GLFW_KEY_TAB ) == GLFW_PRESS){
		//flag = TORSO;
		static int cnt = 0;
		if(cnt==1)
			return;
		for(int index=0; index<transformersList.size(); index++){
			(*transformersList[index]).bumbleTorso.leftThighTPivot.rotateLeftThighTPivot(toradians(90.0f),vec3(1,0,0),(*transformersList[index]));
			(*transformersList[index]).bumbleTorso.leftShoulderPivot.rotateLeftShoulderPivot(toradians(90.0f),vec3(0,1,0),*transformersList[index]);
			(*transformersList[index]).bumbleTorso.rightShoulderPivot.rotateRightShoulderPivot(toradians(-90.0f),vec3(0,1,0),*transformersList[index]);
		}
		cnt++;
	}
	
	if (glfwGetKey( mainwindow, GLFW_KEY_B ) == GLFW_PRESS){
		static int step = 0; 
		if (!(*Globals::activeTransformer).car) {
			if(step==0)
				(*Globals::activeTransformer).bumbleTorso.leftThighTPivot.rotateLeftThighTPivot(toradians(90.0f),vec3(1,0,0),(*Globals::activeTransformer));
			(*Globals::activeTransformer).bumbleTorso.leftShoulderPivot.rotateLeftShoulderPivot(toradians(90.0f),vec3(0,1,0),(*Globals::activeTransformer));
			(*Globals::activeTransformer).bumbleTorso.rightShoulderPivot.rotateRightShoulderPivot(toradians(-90.0f),vec3(0,1,0),(*Globals::activeTransformer));
			(*Globals::activeTransformer).bumbleLeftArm.leftWristPivot.rotateLeftWristPivot(toradians(90.0f),vec3(0,0,1),(*Globals::activeTransformer));
			(*Globals::activeTransformer).bumbleRightArm.rightWristPivot.rotateRightWristPivot(toradians(-90.0f),vec3(0,0,1),(*Globals::activeTransformer));
			(*Globals::activeTransformer).bumbleTorso.leftThighPivot.rotateLeftThighPivot(toradians(90.0f),vec3(1,0,0),true,(*Globals::activeTransformer));
			(*Globals::activeTransformer).bumbleTorso.rightThighPivot.rotateRightThighPivot(toradians(90.0f),vec3(1,0,0),true,(*Globals::activeTransformer));
			(*Globals::activeTransformer).bumbleLeftThigh.leftLegPivot.rotateLeftLegPivot(toradians(-180.0f),vec3(1,0,0),(*Globals::activeTransformer));
			(*Globals::activeTransformer).bumbleRightThigh.rightLegPivot.rotateRightLegPivot(toradians(-180.0f),vec3(1,0,0),(*Globals::activeTransformer)); 
			Globals::mode = 1;
			(*Globals::activeTransformer).bumbleTorso.translate(vec3(0,0,16),(*Globals::activeTransformer));

			(*Globals::activeTransformer).car = true;
		}
	}
	
	if (glfwGetKey( mainwindow, GLFW_KEY_0 ) == GLFW_PRESS){ 
		static vector<Transformer*>::iterator transiterate = transformersList.begin();
		Globals::activeTransformer = &(**transiterate);
		Globals::activeIndex = transiterate - transformersList.begin();

		transiterate++;

		if (transiterate == transformersList.end())
			transiterate = transformersList.begin();
	}
	
	if (glfwGetKey( mainwindow, GLFW_KEY_1 ) == GLFW_PRESS){
		static int cnt=0;

		if (cnt==1)
			return;


		(*Globals::activeTransformer).car = true;
	
		Globals::mode = 1;
		(*Globals::activeTransformer).bumbleTorso.translate(vec3(0,0,16),(*Globals::activeTransformer));
		Globals::headlights = 1;
		Globals::streetlights = 1;
		flag = CAR;
		if (cnt == 0) {
			Globals::init_cameras();
			cnt++;
		}
		
	} 
	
	if (glfwGetKey( mainwindow, GLFW_KEY_2 ) == GLFW_PRESS){
		flag = DIRCAM;
	}
	
	if (glfwGetKey( mainwindow, GLFW_KEY_3 ) == GLFW_PRESS){
		flag = DIRCAMR;
	}

	if (glfwGetKey( mainwindow, GLFW_KEY_LEFT_SHIFT ) == GLFW_PRESS){
		if (dir==1)
			dir = -1;
		else
			dir = 1;
	}
	
	if (glfwGetKey( mainwindow, GLFW_KEY_F ) == GLFW_PRESS){
		Globals::currentMode = 1;
	}
	
	if (glfwGetKey( mainwindow, GLFW_KEY_C ) == GLFW_PRESS){
		Globals::currentMode = 2;
	}
	
	if (glfwGetKey( mainwindow, GLFW_KEY_G ) == GLFW_PRESS){
		Globals::currentMode = 0;
	}
	
	if (glfwGetKey( mainwindow, GLFW_KEY_V ) == GLFW_PRESS){
		Globals::currentMode = 3;
	}
	
	
	
	if (glfwGetKey( mainwindow, GLFW_KEY_ENTER ) == GLFW_PRESS){
		static int cnt = 0;
		if(cnt==1)
			return;
		Globals::currentMode = 2;
		cnt++;
		animate();
	}

	if (glfwGetKey( mainwindow, GLFW_KEY_UP ) == GLFW_PRESS && Globals::mode==1){
		printf("car moved\n");
		dir = 1;
		(*Globals::activeTransformer).bumbleTorso.translate(vec3(0,deltaTime*speed,0),(*Globals::activeTransformer));
		(*Globals::activeTransformer).bumbleLeftShoulder.leftShoulderWheelPivot.rotateLeftShoulderWheelPivot(toradians(5.0f),vec3(0,0,1),(*Globals::activeTransformer));
		(*Globals::activeTransformer).bumbleRightShoulder.rightShoulderWheelPivot.rotateRightShoulderWheelPivot(toradians(-5.0f),vec3(0,0,1),(*Globals::activeTransformer));
		(*Globals::activeTransformer).bumbleLeftArm.leftArmWheelPivot.rotateLeftArmWheelPivot(toradians(5.0f),vec3(0,0,1),(*Globals::activeTransformer));
		(*Globals::activeTransformer).bumbleRightArm.rightArmWheelPivot.rotateRightArmWheelPivot(toradians(-5.0f),vec3(0,0,1),(*Globals::activeTransformer));
	}

	if (glfwGetKey( mainwindow, GLFW_KEY_RIGHT ) == GLFW_PRESS && Globals::mode==1){
		(*Globals::activeTransformer).bumbleTorso.rotate(dir*speeda*toradians(0.1f),(*Globals::activeTransformer));
	}
	
	if (glfwGetKey( mainwindow, GLFW_KEY_LEFT ) == GLFW_PRESS && Globals::mode==1){
		(*Globals::activeTransformer).bumbleTorso.rotate(dir*speeda*toradians(-0.1f),(*Globals::activeTransformer));
	}
	
	if (glfwGetKey( mainwindow, GLFW_KEY_DOWN ) == GLFW_PRESS && Globals::mode==1){
		dir = -1;

		(*Globals::activeTransformer).bumbleTorso.translate(vec3(0,-deltaTime*speed,0),(*Globals::activeTransformer));
		(*Globals::activeTransformer).bumbleLeftShoulder.leftShoulderWheelPivot.rotateLeftShoulderWheelPivot(toradians(-5.0f),vec3(0,0,1),(*Globals::activeTransformer));
		(*Globals::activeTransformer).bumbleRightShoulder.rightShoulderWheelPivot.rotateRightShoulderWheelPivot(toradians(5.0f),vec3(0,0,1),(*Globals::activeTransformer));
		(*Globals::activeTransformer).bumbleLeftArm.leftArmWheelPivot.rotateLeftArmWheelPivot(toradians(-5.0f),vec3(0,0,1),(*Globals::activeTransformer));
		(*Globals::activeTransformer).bumbleRightArm.rightArmWheelPivot.rotateRightArmWheelPivot(toradians(5.0f),vec3(0,0,1),(*Globals::activeTransformer));
	}

	if (glfwGetKey( mainwindow, GLFW_KEY_X ) == GLFW_PRESS){
		switch(flag){
			case HEAD:
				(*Globals::activeTransformer).bumbleTorso.neckPivot.rotateNeckPivot(dir*toradians(0.1f),vec3(1,0,0), (*Globals::activeTransformer));			
				break;
			case LEFTHAND:
				(*Globals::activeTransformer).bumbleLeftArm.leftWristPivot.rotateLeftWristPivot(dir*toradians(0.1f),vec3(1,0,0),(*Globals::activeTransformer));
				break;
			case RIGHTHAND:
				(*Globals::activeTransformer).bumbleRightArm.rightWristPivot.rotateRightWristPivot(dir*toradians(0.1f),vec3(1,0,0),(*Globals::activeTransformer));
				break;
			case LEFTFOOT:
				(*Globals::activeTransformer).bumbleLeftLeg.leftFootPivot.rotateLeftFootPivot(dir*toradians(0.1f),vec3(1,0,0),(*Globals::activeTransformer));
				break;
			case RIGHTFOOT:
				(*Globals::activeTransformer).bumbleRightLeg.rightFootPivot.rotateRightFootPivot(dir*toradians(0.1f),vec3(1,0,0),(*Globals::activeTransformer));
				break;
			case LEFTARM:
				(*Globals::activeTransformer).bumbleLeftShoulder.leftArmPivot.rotateLeftArmPivot(dir*toradians(0.1f),vec3(1,0,0),(*Globals::activeTransformer));
				break;
			case RIGHTARM:
				(*Globals::activeTransformer).bumbleRightShoulder.rightArmPivot.rotateRightArmPivot(dir*toradians(0.1f),vec3(1,0,0),(*Globals::activeTransformer));
				break;
			case LEFTSHOULDER:
				(*Globals::activeTransformer).bumbleTorso.leftShoulderPivot.rotateLeftShoulderPivot(dir*toradians(0.1f),vec3(1,0,0),(*Globals::activeTransformer));
				break;
			case RIGHTSHOULDER:
				(*Globals::activeTransformer).bumbleTorso.rightShoulderPivot.rotateRightShoulderPivot(dir*toradians(0.1f),vec3(1,0,0),(*Globals::activeTransformer));
				break;
			case LEFTLEG:
				(*Globals::activeTransformer).bumbleLeftThigh.leftLegPivot.rotateLeftLegPivot(dir*toradians(0.1f),vec3(1,0,0),(*Globals::activeTransformer));
				break;
			case RIGHTLEG:
				(*Globals::activeTransformer).bumbleRightThigh.rightLegPivot.rotateRightLegPivot(dir*toradians(0.1f),vec3(1,0,0),(*Globals::activeTransformer));
				break;
			case LEFTTHIGH:
				(*Globals::activeTransformer).bumbleTorso.leftThighPivot.rotateLeftThighPivot(dir*toradians(0.1f),vec3(1,0,0),true,(*Globals::activeTransformer));
				break;
			case RIGHTTHIGH:
				(*Globals::activeTransformer).bumbleTorso.rightThighPivot.rotateRightThighPivot(dir*toradians(0.1f),vec3(1,0,0),true,(*Globals::activeTransformer));
				break;
			case TORSO:
				(*Globals::activeTransformer).bumbleTorso.leftThighTPivot.rotateLeftThighTPivot(dir*toradians(0.1f),vec3(1,0,0),(*Globals::activeTransformer));
				break;
			case DIRCAM:
				Globals::dir_cam_translate(vec3(dir*0.1,0,0));
				break;
			case DIRCAMR:
				Globals::dir_cam_rotate(dir*toradians(0.1f),vec3(1,0,0));
				break;
		}
	}
	
	if (glfwGetKey( mainwindow, GLFW_KEY_Y ) == GLFW_PRESS){
		switch(flag){
			case HEAD:
				(*Globals::activeTransformer).bumbleTorso.neckPivot.rotateNeckPivot(dir*toradians(0.1f),vec3(0,1,0), (*Globals::activeTransformer));			
				break;
			case LEFTFOOT:
				(*Globals::activeTransformer).bumbleLeftLeg.leftFootPivot.rotateLeftFootPivot(dir*toradians(0.1f),vec3(0,1,0),(*Globals::activeTransformer));
				break;
			case RIGHTFOOT:
				(*Globals::activeTransformer).bumbleRightLeg.rightFootPivot.rotateRightFootPivot(dir*toradians(0.1f),vec3(0,1,0),(*Globals::activeTransformer));
				break;
			case LEFTSHOULDER:
				(*Globals::activeTransformer).bumbleTorso.leftShoulderPivot.rotateLeftShoulderPivot(dir*toradians(0.1f),vec3(0,1,0),(*Globals::activeTransformer));
				break;
			case RIGHTSHOULDER:
				(*Globals::activeTransformer).bumbleTorso.rightShoulderPivot.rotateRightShoulderPivot(dir*toradians(0.1f),vec3(0,1,0),(*Globals::activeTransformer));
				break;
			case LEFTHAND:
				(*Globals::activeTransformer).bumbleLeftArm.leftWristPivot.rotateLeftWristPivot(dir*toradians(0.1f),vec3(0,1,0),(*Globals::activeTransformer));
				break;
			case RIGHTHAND:
				(*Globals::activeTransformer).bumbleRightArm.rightWristPivot.rotateRightWristPivot(dir*toradians(0.1f),vec3(0,1,0),(*Globals::activeTransformer));
				break;
			case LEFTTHIGH:
				(*Globals::activeTransformer).bumbleTorso.leftThighPivot.rotateLeftThighPivot(dir*toradians(0.1f),vec3(0,1,0),true,(*Globals::activeTransformer));
				break;
			case RIGHTTHIGH:
				(*Globals::activeTransformer).bumbleTorso.rightThighPivot.rotateRightThighPivot(dir*toradians(0.1f),vec3(0,1,0),true,(*Globals::activeTransformer));
				break;
			case LEFTLEG:
				(*Globals::activeTransformer).bumbleLeftThigh.leftLegPivot.rotateLeftLegPivot(dir*toradians(0.1f),vec3(0,1,0),(*Globals::activeTransformer));
				break;
			case RIGHTLEG:
				(*Globals::activeTransformer).bumbleRightThigh.rightLegPivot.rotateRightLegPivot(dir*toradians(0.1f),vec3(0,1,0),(*Globals::activeTransformer));
				break;
			case TORSO:
				(*Globals::activeTransformer).bumbleTorso.leftThighTPivot.rotateLeftThighTPivot(dir*toradians(0.1f),vec3(0,1,0),(*Globals::activeTransformer));
				break;
			case LEFTARM:
				(*Globals::activeTransformer).bumbleLeftShoulder.leftArmPivot.rotateLeftArmPivot(dir*toradians(0.1f),vec3(0,1,0),(*Globals::activeTransformer));
				break;
			case RIGHTARM:
				(*Globals::activeTransformer).bumbleRightShoulder.rightArmPivot.rotateRightArmPivot(dir*toradians(0.1f),vec3(0,1,0),(*Globals::activeTransformer));
				break;
			case DIRCAM:
				Globals::dir_cam_translate(vec3(0,dir*0.1,0));
				break;
			case DIRCAMR:
				Globals::dir_cam_rotate(dir*toradians(0.1f),vec3(0,1,0));
				break;
		}
	}

	if (glfwGetKey( mainwindow, GLFW_KEY_Z ) == GLFW_PRESS){
		switch(flag){
			case HEAD:
				(*Globals::activeTransformer).bumbleTorso.neckPivot.rotateNeckPivot(dir*toradians(0.1f),vec3(0,0,1),(*Globals::activeTransformer));			
				break;
			case LEFTSHOULDER:
				(*Globals::activeTransformer).bumbleTorso.leftShoulderPivot.rotateLeftShoulderPivot(dir*toradians(0.1f),vec3(0,0,1),(*Globals::activeTransformer));
				break;
			case RIGHTSHOULDER:
				(*Globals::activeTransformer).bumbleTorso.rightShoulderPivot.rotateRightShoulderPivot(dir*toradians(0.1f),vec3(0,0,1),(*Globals::activeTransformer));
				break;
			case LEFTTHIGH:
				(*Globals::activeTransformer).bumbleTorso.leftThighPivot.rotateLeftThighPivot(dir*toradians(0.1f),vec3(0,0,1),true,(*Globals::activeTransformer));
				break;
			case RIGHTTHIGH:
				(*Globals::activeTransformer).bumbleTorso.rightThighPivot.rotateRightThighPivot(dir*toradians(0.1f),vec3(0,0,1),true,(*Globals::activeTransformer));
				break;
			case LEFTFOOT:
				(*Globals::activeTransformer).bumbleLeftLeg.leftFootPivot.rotateLeftFootPivot(dir*toradians(0.1f),vec3(0,0,1),(*Globals::activeTransformer));
				break;
			case RIGHTFOOT:
				(*Globals::activeTransformer).bumbleRightLeg.rightFootPivot.rotateRightFootPivot(dir*toradians(0.1f),vec3(0,0,1),(*Globals::activeTransformer));
				break;
			case TORSO:
				(*Globals::activeTransformer).bumbleTorso.leftThighTPivot.rotateLeftThighTPivot(dir*toradians(0.1f),vec3(0,0,1),(*Globals::activeTransformer));
				break;
			case LEFTHAND:
				(*Globals::activeTransformer).bumbleLeftArm.leftWristPivot.rotateLeftWristPivot(dir*toradians(0.1f),vec3(0,0,1),(*Globals::activeTransformer));
				break;
			case RIGHTHAND:
				(*Globals::activeTransformer).bumbleRightArm.rightWristPivot.rotateRightWristPivot(dir*toradians(0.1f),vec3(0,0,1),(*Globals::activeTransformer));
				break;
			case LEFTARM:
				(*Globals::activeTransformer).bumbleLeftShoulder.leftArmPivot.rotateLeftArmPivot(dir*toradians(0.1f),vec3(0,0,1),(*Globals::activeTransformer));
				break;
			case RIGHTARM:
				(*Globals::activeTransformer).bumbleRightShoulder.rightArmPivot.rotateRightArmPivot(dir*toradians(0.1f),vec3(0,0,1),(*Globals::activeTransformer));
				break;
			case LEFTLEG:
				(*Globals::activeTransformer).bumbleLeftThigh.leftLegPivot.rotateLeftLegPivot(dir*toradians(0.1f),vec3(0,0,1),(*Globals::activeTransformer));
				break;
			case RIGHTLEG:
				(*Globals::activeTransformer).bumbleRightThigh.rightLegPivot.rotateRightLegPivot(dir*toradians(0.1f),vec3(0,0,1),(*Globals::activeTransformer));
				break;
			case DIRCAM:
				Globals::dir_cam_translate(vec3(0,0,dir*0.1));
				break;
			case DIRCAMR:
				Globals::dir_cam_rotate(dir*toradians(0.1f),vec3(0,0,1));
				break;
		}
	}

	lastTime = currentTime;
}
