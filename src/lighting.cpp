#include "../includes/lighting.h"
#include <string>
#include <string.h>
#include <stdio.h>
using namespace std;

bool add_light_source(vec3 _Ld, vec3 _location, unsigned int _type, int fixed){
		if(count > 50)
			return false;
		light_sources[count].Ld[0] = _Ld.x;
		light_sources[count].Ld[1] = _Ld.y;
		light_sources[count].Ld[2] = _Ld.z;
		light_sources[count].Ld[3] = 1;
		
		light_sources[count].location[0] = _location.x;
		light_sources[count].location[1] = _location.y;
		light_sources[count].location[2] = _location.z;
		light_sources[count].location[3] = fixed;

		light_sources[count].type = _type;
		if(light_sources[count].type == 2){
			light_sources[count].Ls[0] = 1; 
			light_sources[count].Ls[1] = 1; 
			light_sources[count].Ls[2] = 1; 
			light_sources[count].Ls[3] = 0; 
		}else{
			light_sources[count].Ls[0] = 0;
			light_sources[count].Ls[1] = 0;
			light_sources[count].Ls[2] = 0;
			light_sources[count].Ls[3] = 0;
		}

		light_sources[count].radii = 200.0f;
		light_sources[count].specular_exponent = 1.0f;
		++count;
		return true;
	}

bool add_light_source_full(vec3 _Ld, vec3 _location, unsigned int _type, float _radii, float _specular_exponent, int fixed){
		if(count > 50)
			return false;
		light_sources[count].Ld[0] = _Ld.x;
		light_sources[count].Ld[1] = _Ld.y;
		light_sources[count].Ld[2] = _Ld.z;
		light_sources[count].Ld[3] = 1;

		light_sources[count].location[0] = _location.x;
		light_sources[count].location[1] = _location.y;
		light_sources[count].location[2] = _location.z;
		light_sources[count].location[3] = fixed;

		light_sources[count].type = _type;
		if(light_sources[count].type == 2){
			light_sources[count].Ls[0] = 1; 
			light_sources[count].Ls[1] = 1; 
			light_sources[count].Ls[2] = 1; 
			light_sources[count].Ls[3] = 0; 
		}else{
			light_sources[count].Ls[0] = 0;
			light_sources[count].Ls[1] = 0;
			light_sources[count].Ls[2] = 0;
			light_sources[count].Ls[3] = 0;
		}
		light_sources[count].radii = _radii;
		light_sources[count].specular_exponent = _specular_exponent;
		++count;
		return true;
	}

void light_setup(){
	string name("light_sources[");
	string name2("].");
	string finname("");
	string member("");
	lightcountId = glGetUniformLocation(Globals::ProgramId, "count");

	for(int i=0;i<count;i++){
		string index = to_string(i);

		finname="";
		member="Ld";
		finname = name+index+name2+member;
		light_sources_ids[i].Ld = glGetUniformLocation(Globals::ProgramId, finname.c_str());
		
		finname="";
		member="Ls";
		finname = name+index+name2+member;
		light_sources_ids[i].Ls = glGetUniformLocation(Globals::ProgramId, finname.c_str());

		finname="";
		member="location";
		finname = name+index+name2+member;
		light_sources_ids[i].location = glGetUniformLocation(Globals::ProgramId, finname.c_str());

		finname="";
		member="radii";
		finname = name+index+name2+member;
		light_sources_ids[i].radii = glGetUniformLocation(Globals::ProgramId, finname.c_str());

		finname="";
		member="specular_exponent";
		finname = name+index+name2+member;
		light_sources_ids[i].specular_exponent = glGetUniformLocation(Globals::ProgramId, finname.c_str());
	}
}

void push_lights(){

	if(Globals::streetlights && Globals::headlights){
		glUniform1i(lightcountId,count);
	}
	if(Globals::streetlights && !Globals::headlights){
		glUniform1i(lightcountId,count - 2);
	}
	if(!Globals::streetlights && Globals::headlights){
		glUniform1i(lightcountId,2);
	}
	if(!Globals::streetlights && !Globals::headlights){
		glUniform1i(lightcountId,0);
	}

	int i=0;
	int j;
	if(Globals::streetlights) {
		for(i=0;i<count-2;i++){
			glUniform4fv(light_sources_ids[i].Ld,1,light_sources[i].Ld);
			glUniform4fv(light_sources_ids[i].Ls,1,light_sources[i].Ls);
			glUniform4fv(light_sources_ids[i].location,1,light_sources[i].location);
			glUniform1f(light_sources_ids[i].radii,light_sources[i].radii);
			glUniform1f(light_sources_ids[i].specular_exponent,light_sources[i].specular_exponent);
		}
	}

	if(Globals::headlights) {
		if (Globals::streetlights) {
			j = i;
		}else{
			j = count - 2;
			i = 0;
		}

		glUniform4fv(light_sources_ids[i].Ld,1,light_sources[j].Ld);
		glUniform4fv(light_sources_ids[i].Ls,1,light_sources[j].Ls);
		
		vec3 carPos = vec3(Globals::TransformerMatrix[3]);
		vec3 head1 = carPos+Globals::head1off;
		head1pos[0] = head1.x;
		head1pos[1] = head1.y;
		head1pos[2] = head1.z;
		head1pos[3] = 1;
		glUniform4fv(light_sources_ids[i].location,1,head1pos);
		glUniform1f(light_sources_ids[i].radii,light_sources[j].radii);
		glUniform1f(light_sources_ids[i].specular_exponent,light_sources[j].specular_exponent);

		i++;	
		glUniform4fv(light_sources_ids[i].Ld,1,light_sources[j].Ld);
		glUniform4fv(light_sources_ids[i].Ls,1,light_sources[j].Ls);
		
		vec3 head2 = carPos+Globals::head2off;
		head2pos[0] = head2.x;
		head2pos[1] = head2.y;
		head2pos[2] = head2.z;
		head2pos[3] = 1;
		glUniform4fv(light_sources_ids[i].location,1,head2pos);
		glUniform1f(light_sources_ids[i].radii,light_sources[j].radii);
		glUniform1f(light_sources_ids[i].specular_exponent,light_sources[j].specular_exponent);
	}
	
}
