#include<cmath>
#include "../includes/transformer.h"
#define GLM_FORCE_RADIANS
#include "../glm/glm/glm.hpp"
#include "../glm/glm/gtc/matrix_transform.hpp"
#include <math.h>
#include "../includes/transformer.h"

using namespace glm;


/*extern Torso bumbleTorso;
extern Head bumbleHead;
extern LeftShoulder bumbleLeftShoulder;
extern RightShoulder bumbleRightShoulder;
extern LeftArm bumbleLeftArm;
extern RightArm bumbleRightArm;
extern LeftHand bumbleLeftHand;
extern RightHand bumbleRightHand;
extern LeftThigh bumbleLeftThigh;
extern RightThigh bumbleRightThigh;
extern LeftLeg bumbleLeftLeg;
extern RightLeg bumbleRightLeg;
extern LeftFoot bumbleLeftFoot;
extern RightFoot bumbleRightFoot;
extern LeftShoulderWheel bumbleLeftShoulderWheel;
extern RightShoulderWheel bumbleRightShoulderWheel;
extern LeftArmWheel bumbleLeftArmWheel;
extern RightArmWheel bumbleRightArmWheel;
*/
void SinglePivot::rotateNeckPivot(float angle,vec3 axis, Transformer &t){
	rotation robj = rotation(angle,axis);
	rotations.push_back(robj);

	mat4 pivotMat = t.bumbleTorso.neckPivot.getModelMatrix();
	pivotMat = rotate(pivotMat,angle,axis);
	t.bumbleTorso.neckPivot.setModelMatrix(pivotMat);

	t.bumbleHead.readjust(t.bumbleTorso.neckPivot.getModelMatrix());

	if(axis==vec3(1,0,0))
		rotX+=angle;
	if(axis==vec3(0,1,0))
		rotY+=angle;
	if(axis==vec3(0,0,1))
		rotZ+=angle;
}

void SinglePivot::rotateLeftShoulderPivot(float angle,vec3 axis, Transformer &t){
	rotation robj = rotation(angle,axis);
	rotations.push_back(robj);

	mat4 pivotMat = t.bumbleTorso.leftShoulderPivot.getModelMatrix();
	pivotMat = rotate(pivotMat,angle,axis);
	t.bumbleTorso.leftShoulderPivot.setModelMatrix(pivotMat);

	t.bumbleLeftShoulder.readjust(t.bumbleTorso.leftShoulderPivot.getModelMatrix(),t);
	
	if(axis==vec3(1,0,0))
		rotX+=angle;
	if(axis==vec3(0,1,0))
		rotY+=angle;
	if(axis==vec3(0,0,1))
		rotZ+=angle;
}

void SinglePivot::rotateRightShoulderPivot(float angle,vec3 axis, Transformer &t){
	rotation robj = rotation(angle,axis);
	rotations.push_back(robj);

	mat4 pivotMat = t.bumbleTorso.rightShoulderPivot.getModelMatrix();
	pivotMat = rotate(pivotMat,angle,axis);
	t.bumbleTorso.rightShoulderPivot.setModelMatrix(pivotMat);

	t.bumbleRightShoulder.readjust(t.bumbleTorso.rightShoulderPivot.getModelMatrix(),t);
	
	if(axis==vec3(1,0,0))
		rotX+=angle;
	if(axis==vec3(0,1,0))
		rotY+=angle;
	if(axis==vec3(0,0,1))
		rotZ+=angle;
}

void SinglePivot::rotateLeftArmPivot(float angle,vec3 axis, Transformer &t){
	rotation robj = rotation(angle,axis);
	rotations.push_back(robj);

	mat4 pivotMat = t.bumbleLeftShoulder.leftArmPivot.getModelMatrix();
	pivotMat = rotate(pivotMat,angle,axis);
	t.bumbleLeftShoulder.leftArmPivot.setModelMatrix(pivotMat);

	t.bumbleLeftArm.readjust(t.bumbleLeftShoulder.leftArmPivot.getModelMatrix(),t);
	
	if(axis==vec3(1,0,0))
		rotX+=angle;
	if(axis==vec3(0,1,0))
		rotY+=angle;
	if(axis==vec3(0,0,1))
		rotZ+=angle;
}

void SinglePivot::rotateRightArmPivot(float angle,vec3 axis, Transformer &t){
	rotation robj = rotation(angle,axis);
	rotations.push_back(robj);

	mat4 pivotMat = t.bumbleRightShoulder.rightArmPivot.getModelMatrix();
	pivotMat = rotate(pivotMat,angle,axis);
	t.bumbleRightShoulder.rightArmPivot.setModelMatrix(pivotMat);

	t.bumbleRightArm.readjust(t.bumbleRightShoulder.rightArmPivot.getModelMatrix(),t);
	
	if(axis==vec3(1,0,0))
		rotX+=angle;
	if(axis==vec3(0,1,0))
		rotY+=angle;
	if(axis==vec3(0,0,1))
		rotZ+=angle;
}

void SinglePivot::rotateLeftWristPivot(float angle,vec3 axis, Transformer &t){
	rotation robj = rotation(angle,axis);
	rotations.push_back(robj);

	mat4 pivotMat = t.bumbleLeftArm.leftWristPivot.getModelMatrix();
	pivotMat = rotate(pivotMat,angle,axis);
	t.bumbleLeftArm.leftWristPivot.setModelMatrix(pivotMat);

	t.bumbleLeftHand.readjust(t.bumbleLeftArm.leftWristPivot.getModelMatrix());
	
	if(axis==vec3(1,0,0))
		rotX+=angle;
	if(axis==vec3(0,1,0))
		rotY+=angle;
	if(axis==vec3(0,0,1))
		rotZ+=angle;
}

void SinglePivot::rotateRightWristPivot(float angle,vec3 axis, Transformer &t){
	rotation robj = rotation(angle,axis);
	rotations.push_back(robj);

	mat4 pivotMat = t.bumbleRightArm.rightWristPivot.getModelMatrix();
	pivotMat = rotate(pivotMat,angle,axis);
	t.bumbleRightArm.rightWristPivot.setModelMatrix(pivotMat);

	t.bumbleRightHand.readjust(t.bumbleRightArm.rightWristPivot.getModelMatrix());
	
	if(axis==vec3(1,0,0))
		rotX+=angle;
	if(axis==vec3(0,1,0))
		rotY+=angle;
	if(axis==vec3(0,0,1))
		rotZ+=angle;
}

void SinglePivot::rotateLeftThighPivot(float angle,vec3 axis,bool leg, Transformer &t){
	printf("rotate left thigh p\n");
	rotation robj = rotation(angle,axis,leg);
	rotations.push_back(robj);

	mat4 pivotMat = t.bumbleTorso.leftThighPivot.getModelMatrix();
	pivotMat = rotate(pivotMat,angle,axis);
	t.bumbleTorso.leftThighPivot.setModelMatrix(pivotMat);

	t.bumbleLeftThigh.readjust(t.bumbleTorso.leftThighPivot.getModelMatrix(),t);

	if(axis==vec3(1,0,0))
		rotX+=angle;
	if(axis==vec3(0,1,0))
		rotY+=angle;
	if(axis==vec3(0,0,1))
		rotZ+=angle;
}

void SinglePivot::rotateLeftThighTPivot(float angle,vec3 axis, Transformer &t){
	printf("rotate left thigh t p\n");
	rotation robj = rotation(angle,axis);
	rotations.push_back(robj);

	mat4 pivotMat = t.bumbleTorso.leftThighTPivot.getModelMatrix();
	pivotMat = rotate(pivotMat,angle,axis);
	t.bumbleTorso.leftThighTPivot.setModelMatrix(pivotMat);

	t.bumbleTorso.readjust(t.bumbleTorso.leftThighTPivot.getModelMatrix(), t);

	if(axis==vec3(1,0,0))
		rotX+=angle;
	if(axis==vec3(0,1,0))
		rotY+=angle;
	if(axis==vec3(0,0,1))
		rotZ+=angle;
}

void SinglePivot::rotateRightThighPivot(float angle,vec3 axis,bool leg, Transformer &t){
	rotation robj = rotation(angle,axis,leg);
	rotations.push_back(robj);

	mat4 pivotMat = t.bumbleTorso.rightThighPivot.getModelMatrix();
	pivotMat = rotate(pivotMat,angle,axis);
	t.bumbleTorso.rightThighPivot.setModelMatrix(pivotMat);

	t.bumbleRightThigh.readjust(t.bumbleTorso.rightThighPivot.getModelMatrix(),t);
	
	if(axis==vec3(1,0,0))
		rotX+=angle;
	if(axis==vec3(0,1,0))
		rotY+=angle;
	if(axis==vec3(0,0,1))
		rotZ+=angle;
}

void SinglePivot::rotateLeftLegPivot(float angle,vec3 axis, Transformer &t){
	rotation robj = rotation(angle,axis);
	rotations.push_back(robj);

	mat4 pivotMat = t.bumbleLeftThigh.leftLegPivot.getModelMatrix();
	pivotMat = rotate(pivotMat,angle,axis);
	t.bumbleLeftThigh.leftLegPivot.setModelMatrix(pivotMat);

	t.bumbleLeftLeg.readjust(t.bumbleLeftThigh.leftLegPivot.getModelMatrix(),t);
	
	if(axis==vec3(1,0,0))
		rotX+=angle;
	if(axis==vec3(0,1,0))
		rotY+=angle;
	if(axis==vec3(0,0,1))
		rotZ+=angle;
}

void SinglePivot::rotateRightLegPivot(float angle,vec3 axis, Transformer &t){
	rotation robj = rotation(angle,axis);
	rotations.push_back(robj);

	mat4 pivotMat = t.bumbleRightThigh.rightLegPivot.getModelMatrix();
	pivotMat = rotate(pivotMat,angle,axis);
	t.bumbleRightThigh.rightLegPivot.setModelMatrix(pivotMat);

	t.bumbleRightLeg.readjust(t.bumbleRightThigh.rightLegPivot.getModelMatrix(),t);
	
	if(axis==vec3(1,0,0))
		rotX+=angle;
	if(axis==vec3(0,1,0))
		rotY+=angle;
	if(axis==vec3(0,0,1))
		rotZ+=angle;
}

void SinglePivot::rotateLeftFootPivot(float angle,vec3 axis, Transformer &t){
	rotation robj = rotation(angle,axis);
	rotations.push_back(robj);

	mat4 pivotMat = t.bumbleLeftLeg.leftFootPivot.getModelMatrix();
	pivotMat = rotate(pivotMat,angle,axis);
	t.bumbleLeftLeg.leftFootPivot.setModelMatrix(pivotMat);

	t.bumbleLeftFoot.readjust(t.bumbleLeftLeg.leftFootPivot.getModelMatrix());
	
	if(axis==vec3(1,0,0))
		rotX+=angle;
	if(axis==vec3(0,1,0))
		rotY+=angle;
	if(axis==vec3(0,0,1))
		rotZ+=angle;
}

void SinglePivot::rotateRightFootPivot(float angle,vec3 axis, Transformer &t){
	rotation robj = rotation(angle,axis);
	rotations.push_back(robj);

	mat4 pivotMat = t.bumbleRightLeg.rightFootPivot.getModelMatrix();
	pivotMat = rotate(pivotMat,angle,axis);
	t.bumbleRightLeg.rightFootPivot.setModelMatrix(pivotMat);

	t.bumbleRightFoot.readjust(t.bumbleRightLeg.rightFootPivot.getModelMatrix());
	
	if(axis==vec3(1,0,0))
		rotX+=angle;
	if(axis==vec3(0,1,0))
		rotY+=angle;
	if(axis==vec3(0,0,1))
		rotZ+=angle;
}

void SinglePivot::rotateLeftShoulderWheelPivot(float angle,vec3 axis, Transformer &t){
	rotation robj = rotation(angle,axis);
	rotations.push_back(robj);

	mat4 pivotMat = t.bumbleLeftShoulder.leftShoulderWheelPivot.getModelMatrix();
	pivotMat = rotate(pivotMat,angle,axis);
	t.bumbleLeftShoulder.leftShoulderWheelPivot.setModelMatrix(pivotMat);

	t.bumbleLeftShoulderWheel.readjust(t.bumbleLeftShoulder.leftShoulderWheelPivot.getModelMatrix());
	
	if(axis==vec3(1,0,0))
		rotX+=angle;
	if(axis==vec3(0,1,0))
		rotY+=angle;
	if(axis==vec3(0,0,1))
		rotZ+=angle;
}

void SinglePivot::rotateRightShoulderWheelPivot(float angle,vec3 axis, Transformer &t){
	rotation robj = rotation(angle,axis);
	rotations.push_back(robj);

	mat4 pivotMat = t.bumbleRightShoulder.rightShoulderWheelPivot.getModelMatrix();
	pivotMat = rotate(pivotMat,angle,axis);
	t.bumbleRightShoulder.rightShoulderWheelPivot.setModelMatrix(pivotMat);

	t.bumbleRightShoulderWheel.readjust(t.bumbleRightShoulder.rightShoulderWheelPivot.getModelMatrix());
	
	if(axis==vec3(1,0,0))
		rotX+=angle;
	if(axis==vec3(0,1,0))
		rotY+=angle;
	if(axis==vec3(0,0,1))
		rotZ+=angle;
}

void SinglePivot::rotateLeftArmWheelPivot(float angle,vec3 axis, Transformer &t){
	rotation robj = rotation(angle,axis);
	rotations.push_back(robj);

	mat4 pivotMat = t.bumbleLeftArm.leftArmWheelPivot.getModelMatrix();
	pivotMat = rotate(pivotMat,angle,axis);
	t.bumbleLeftArm.leftArmWheelPivot.setModelMatrix(pivotMat);

	t.bumbleLeftArmWheel.readjust(t.bumbleLeftArm.leftArmWheelPivot.getModelMatrix());
	
	if(axis==vec3(1,0,0))
		rotX+=angle;
	if(axis==vec3(0,1,0))
		rotY+=angle;
	if(axis==vec3(0,0,1))
		rotZ+=angle;
}
void SinglePivot::rotateRightArmWheelPivot(float angle,vec3 axis, Transformer &t){
	rotation robj = rotation(angle,axis);
	rotations.push_back(robj);

	mat4 pivotMat = t.bumbleRightArm.rightArmWheelPivot.getModelMatrix();
	pivotMat = rotate(pivotMat,angle,axis);
	t.bumbleRightArm.rightArmWheelPivot.setModelMatrix(pivotMat);

	t.bumbleRightArmWheel.readjust(t.bumbleRightArm.rightArmWheelPivot.getModelMatrix());
	
	if(axis==vec3(1,0,0))
		rotX+=angle;
	if(axis==vec3(0,1,0))
		rotY+=angle;
	if(axis==vec3(0,0,1))
		rotZ+=angle;
}


/**************************************************************************/

void SinglePivot::readjustLeftShoulderPivot(mat4 parentMatrix, Transformer &t){
	vector<rotation> oldrotations = rotations;
	rotations.clear();

	mat4 newModel = translate(parentMatrix,offsetFromParent);
	setModelMatrix(newModel);

	if (oldrotations.size()>0) {	
		vector<rotation>::iterator compoiterate = oldrotations.begin();
		
		while(compoiterate != oldrotations.end()){
			rotateLeftShoulderPivot((*compoiterate).angle,(*compoiterate).axis,t);
			compoiterate++;
		}
	} else {
		t.bumbleLeftShoulder.readjust(t.bumbleTorso.leftShoulderPivot.getModelMatrix(),t);
	}
}

void SinglePivot::readjustRightShoulderPivot(mat4 parentMatrix, Transformer &t){
	vector<rotation> oldrotations = rotations;
	rotations.clear();

	mat4 newModel = translate(parentMatrix,offsetFromParent);
	setModelMatrix(newModel);

	if (oldrotations.size() > 0) {	
		vector<rotation>::iterator compoiterate = oldrotations.begin();
		
		while(compoiterate != oldrotations.end()){
			rotateRightShoulderPivot((*compoiterate).angle,(*compoiterate).axis,t);
			compoiterate++;
		}
	} else {
		t.bumbleRightShoulder.readjust(t.bumbleTorso.rightShoulderPivot.getModelMatrix(),t);
	}
}

void SinglePivot::readjustNeckPivot(mat4 parentMatrix, Transformer &t){
	vector<rotation> oldrotations = rotations;
	rotations.clear();

	mat4 newModel = translate(parentMatrix,offsetFromParent);
	setModelMatrix(newModel);

	if ( oldrotations.size() > 0 ) {	
		vector<rotation>::iterator compoiterate = oldrotations.begin();
		
		while(compoiterate != oldrotations.end()){
			rotateNeckPivot((*compoiterate).angle,(*compoiterate).axis, t);
			compoiterate++;
		}
		
	} else {
		t.bumbleHead.readjust(t.bumbleTorso.neckPivot.getModelMatrix());
	}
}

void SinglePivot::readjustLeftArmPivot(mat4 parentMatrix, Transformer &t){
	vector<rotation> oldrotations = rotations;
	rotations.clear();

	mat4 newModel = translate(parentMatrix,offsetFromParent);
	setModelMatrix(newModel);

	if ( oldrotations.size() > 0 ) {	
		vector<rotation>::iterator compoiterate = oldrotations.begin();
		
		while(compoiterate != oldrotations.end()){
			rotateLeftArmPivot((*compoiterate).angle,(*compoiterate).axis,t);
			compoiterate++;
		}
		
	} else {
		t.bumbleLeftArm.readjust(t.bumbleLeftShoulder.leftArmPivot.getModelMatrix(),t);
	}
}

void SinglePivot::readjustRightArmPivot(mat4 parentMatrix, Transformer &t){
	vector<rotation> oldrotations = rotations;
	rotations.clear();

	mat4 newModel = translate(parentMatrix,offsetFromParent);
	setModelMatrix(newModel);

	if ( oldrotations.size() > 0 ) {	
		vector<rotation>::iterator compoiterate = oldrotations.begin();
		
		while(compoiterate != oldrotations.end()){
			rotateRightArmPivot((*compoiterate).angle,(*compoiterate).axis,t);
			compoiterate++;
		}
	} else {
		t.bumbleRightArm.readjust(t.bumbleRightShoulder.rightArmPivot.getModelMatrix(),t);
	}
}

void SinglePivot::readjustLeftWristPivot(mat4 parentMatrix, Transformer &t){
	vector<rotation> oldrotations = rotations;
	rotations.clear();

	mat4 newModel = translate(parentMatrix,offsetFromParent);
	setModelMatrix(newModel);

	if ( oldrotations.size() > 0 ) {	
		vector<rotation>::iterator compoiterate = oldrotations.begin();
		
		while(compoiterate != oldrotations.end()){
			rotateLeftWristPivot((*compoiterate).angle,(*compoiterate).axis,t);
			compoiterate++;
		}
	} else {
		t.bumbleLeftHand.readjust(t.bumbleLeftArm.leftWristPivot.getModelMatrix());
	}
}

void SinglePivot::readjustRightWristPivot(mat4 parentMatrix, Transformer &t){
	vector<rotation> oldrotations = rotations;
	rotations.clear();

	mat4 newModel = translate(parentMatrix,offsetFromParent);
	setModelMatrix(newModel);

	if ( oldrotations.size() > 0 ) {	
		vector<rotation>::iterator compoiterate = oldrotations.begin();
		
		while(compoiterate != oldrotations.end()){
			rotateRightWristPivot((*compoiterate).angle,(*compoiterate).axis,t);
			compoiterate++;
		}
	} else {
		t.bumbleRightHand.readjust(t.bumbleRightArm.rightWristPivot.getModelMatrix());
	}
}

void SinglePivot::readjustRightLegPivot(mat4 parentMatrix, Transformer &t){
	vector<rotation> oldrotations = rotations;
	rotations.clear();

	mat4 newModel = translate(parentMatrix,offsetFromParent);
	setModelMatrix(newModel);

	if ( oldrotations.size() > 0 ) {	
		vector<rotation>::iterator compoiterate = oldrotations.begin();
		
		while(compoiterate != oldrotations.end()){
			rotateRightLegPivot((*compoiterate).angle,(*compoiterate).axis,t);
			compoiterate++;
		}
	} else {
		t.bumbleRightLeg.readjust(t.bumbleRightThigh.rightLegPivot.getModelMatrix(),t);
	}
}

void SinglePivot::readjustLeftLegPivot(mat4 parentMatrix, Transformer &t){
	vector<rotation> oldrotations = rotations;
	rotations.clear();

	printf("readjust left leg\n");
	mat4 newModel = translate(parentMatrix,offsetFromParent);
	setModelMatrix(newModel);

	if ( oldrotations.size() > 0 ) {	
		vector<rotation>::iterator compoiterate = oldrotations.begin();
		
		while(compoiterate != oldrotations.end()){
			rotateLeftLegPivot((*compoiterate).angle,(*compoiterate).axis,t);
			compoiterate++;
		}
	} else {
		t.bumbleLeftLeg.readjust(t.bumbleLeftThigh.leftLegPivot.getModelMatrix(),t);
	}
}

void SinglePivot::readjustLeftFootPivot(mat4 parentMatrix, Transformer &t){
	vector<rotation> oldrotations = rotations;
	rotations.clear();

	mat4 newModel = translate(parentMatrix,offsetFromParent);
	setModelMatrix(newModel);

	if ( oldrotations.size() > 0 ) {	
		vector<rotation>::iterator compoiterate = oldrotations.begin();
		
		while(compoiterate != oldrotations.end()){
			rotateLeftFootPivot((*compoiterate).angle,(*compoiterate).axis,t);
			compoiterate++;
		}
	} else {
		t.bumbleLeftFoot.readjust(t.bumbleLeftLeg.leftFootPivot.getModelMatrix());
	}
}

void SinglePivot::readjustRightFootPivot(mat4 parentMatrix, Transformer &t){
	vector<rotation> oldrotations = rotations;
	rotations.clear();

	mat4 newModel = translate(parentMatrix,offsetFromParent);
	setModelMatrix(newModel);

	if ( oldrotations.size() > 0 ) {	
		vector<rotation>::iterator compoiterate = oldrotations.begin();
		
		while(compoiterate != oldrotations.end()){
			rotateRightFootPivot((*compoiterate).angle,(*compoiterate).axis,t);
			compoiterate++;
		}
	} else {
		t.bumbleRightFoot.readjust(t.bumbleRightLeg.rightFootPivot.getModelMatrix());
	}
}

void SinglePivot::readjustLeftShoulderWheelPivot(mat4 parentMatrix, Transformer &t){
	vector<rotation> oldrotations = rotations;
	rotations.clear();

	mat4 newModel = translate(parentMatrix,offsetFromParent);
	setModelMatrix(newModel);

	if ( oldrotations.size() > 0 ) {	
		
		vector<rotation>::iterator compoiterate = oldrotations.begin();
	
		while(compoiterate != oldrotations.end()){
			rotateLeftShoulderWheelPivot((*compoiterate).angle,(*compoiterate).axis,t);
			compoiterate++;
		}
	} else {
		t.bumbleLeftShoulderWheel.readjust(t.bumbleLeftShoulder.leftShoulderWheelPivot.getModelMatrix());
	}
}

void SinglePivot::readjustRightShoulderWheelPivot(mat4 parentMatrix, Transformer &t){
	vector<rotation> oldrotations = rotations;
	rotations.clear();

	mat4 newModel = translate(parentMatrix,offsetFromParent);
	setModelMatrix(newModel);

	if ( oldrotations.size() > 0 ) {	
		vector<rotation>::iterator compoiterate = oldrotations.begin();
		
		while(compoiterate != oldrotations.end()){
			rotateRightShoulderWheelPivot((*compoiterate).angle,(*compoiterate).axis,t);
			compoiterate++;
		}
	} else {
		t.bumbleRightShoulderWheel.readjust(t.bumbleRightShoulder.rightShoulderWheelPivot.getModelMatrix());
	}
}

void SinglePivot::readjustLeftArmWheelPivot(mat4 parentMatrix, Transformer &t){
	vector<rotation> oldrotations = rotations;
	rotations.clear();

	mat4 newModel = translate(parentMatrix,offsetFromParent);
	setModelMatrix(newModel);

	if ( oldrotations.size() > 0 ) {	
		vector<rotation>::iterator compoiterate = oldrotations.begin();
		
		while(compoiterate != oldrotations.end()){
			rotateLeftArmWheelPivot((*compoiterate).angle,(*compoiterate).axis,t);
			compoiterate++;
		}
	} else {
		t.bumbleLeftArmWheel.readjust(t.bumbleLeftArm.leftArmWheelPivot.getModelMatrix());
	}
}

void SinglePivot::readjustRightArmWheelPivot(mat4 parentMatrix, Transformer &t){
	vector<rotation> oldrotations = rotations;
	rotations.clear();

	mat4 newModel = translate(parentMatrix,offsetFromParent);
	setModelMatrix(newModel);

	if ( oldrotations.size() > 0 ) {	
		vector<rotation>::iterator compoiterate = oldrotations.begin();
		
		while(compoiterate != oldrotations.end()){
			rotateRightArmWheelPivot((*compoiterate).angle,(*compoiterate).axis,t);
			compoiterate++;
		}
	} else {
		t.bumbleRightArmWheel.readjust(t.bumbleRightArm.rightArmWheelPivot.getModelMatrix());
	}
}

void SinglePivot::readjustLeftThighPivot(mat4 parentMatrix, Transformer &t){
	vector<rotation> oldrotations = rotations;
	rotations.clear();

	mat4 newModel = translate(parentMatrix,offsetFromParent);
	setModelMatrix(newModel);

	if(Globals::mode!=1){	
	/*	if (oldrotations.size()>0) {	
			vector<rotation>::iterator compoiterate = oldrotations.begin();
			
			while(compoiterate != oldrotations.end()){
				rotateLeftThighPivot((*compoiterate).angle,(*compoiterate).axis,(*compoiterate).leg,t);
				compoiterate++;
			}
		} else {
			t.bumbleLeftThigh.readjust(t.bumbleTorso.leftThighPivot.getModelMatrix(),t);
		} */
	}else{
		t.bumbleLeftThigh.readjust(t.bumbleTorso.leftThighPivot.getModelMatrix(),t);
	}
}

void SinglePivot::readjustLeftThighTPivot(mat4 parentMatrix, Transformer &t){
	vector<rotation> oldrotations = rotations;
	rotations.clear();

	mat4 newModel = translate(parentMatrix,offsetFromParent);
	setModelMatrix(newModel);
	
	if(Globals::mode!=1){	
		if (oldrotations.size()>0) {	
			vector<rotation>::iterator compoiterate = oldrotations.begin();
			
			while(compoiterate != oldrotations.end()){
				rotateLeftThighTPivot((*compoiterate).angle,(*compoiterate).axis,t);
				compoiterate++;
			}
		} else {
			t.bumbleTorso.readjust(t.bumbleTorso.leftThighTPivot.getModelMatrix(),t);
		}
	}else{
	}
	
}

void SinglePivot::readjustRightThighPivot(mat4 parentMatrix, Transformer &t){
	vector<rotation> oldrotations = rotations;
	rotations.clear();

	mat4 newModel = translate(parentMatrix,offsetFromParent);
	setModelMatrix(newModel);
	
	if(Globals::mode!=1){	
		t.bumbleTorso.rightThighPivot.rotX = 0;
		t.bumbleTorso.rightThighPivot.rotY = 0;
		t.bumbleTorso.rightThighPivot.rotZ = 0;
/*		if (oldrotations.size()>0) {	
			vector<rotation>::iterator compoiterate = oldrotations.begin();
			
			while(compoiterate != oldrotations.end()){
				rotateRightThighPivot((*compoiterate).angle,(*compoiterate).axis,(*compoiterate).leg,t);
				compoiterate++;
			}
		} else {
			t.bumbleRightThigh.readjust(t.bumbleTorso.rightThighPivot.getModelMatrix(),t);
		}  */
	}else{
		t.bumbleRightThigh.readjust(t.bumbleTorso.rightThighPivot.getModelMatrix(),t);
	}
}
