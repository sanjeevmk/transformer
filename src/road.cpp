#include "../includes/scene.h"

void Road::setup(float width,float height,float depth,vec3 position, const char* texture) {
	road.createCuboid(width,height,depth,position,texture);
	components.push_back(road);
}

void Road::render() {
	commonRender();
}
