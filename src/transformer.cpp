#include<GL/glew.h>
#define GLM_FORCE_RADIANS
#include "../glm/glm/glm.hpp"
#include"../includes/framework.hpp"
#include"../includes/models.h"
#include"../includes/globals.h"
#include"../includes/parts.h"
#include "../includes/controls.hpp"
#include "../includes/scene.h"
#include "../includes/lighting.h"
#include "../includes/transformer.h"
#include "../includes/csv.h"

#include <GLFW/glfw3.h>
#include <iostream>
#include<vector>


extern GLFWwindow* mainwindow;

Transformer t1;
Transformer t2;
vector<Transformer*> transformersList;

Skybox skyBox;
Road road1;
Road road2;
Road road3;
Road road4;
Road road5;
Road road6;
Road road7;
Road road8;
Road road9;
Road road10;

const char* bumbletexture = "../images/yellow.bmp";
const char* secondTexture = "../images/grid2.bmp";
const char* eyetexture = "../images/metal.bmp";
const char* pivottexture = "../images/rusted.bmp";
const char* fullhandtexture = "../images/grid2.bmp";
const char* fulllegtexture = "../images/grid2.bmp";
const char* tyretexture = "../images/tyre.bmp";
const char* skytexture = "../images/nightsky.bmp";
const char* bumbletexture2 = "../images/redtexture.bmp";
const char* road1t= "../images/road1.bmp";
const char* road2t= "../images/road2.bmp";

GLuint VertexArrayID;
cuboid testCube;

using namespace glm;

void Transformer::create(vec3 position, const char* bodytexture, const char* limbtexture){
	bumbleTorso.setup(position,bodytexture);
	bumbleHead.setup(bumbleTorso.neckPivot,bodytexture);
	bumbleLeftShoulder.setup(bumbleTorso.leftShoulderPivot,limbtexture);
	bumbleRightShoulder.setup(bumbleTorso.rightShoulderPivot,limbtexture);
	bumbleLeftArm.setup(bumbleLeftShoulder.leftArmPivot,limbtexture);
	bumbleRightArm.setup(bumbleRightShoulder.rightArmPivot,limbtexture);
	bumbleLeftHand.setup(bumbleLeftArm.leftWristPivot,bodytexture);
	bumbleRightHand.setup(bumbleRightArm.rightWristPivot,bodytexture);
	bumbleLeftThigh.setup(bumbleTorso.leftThighPivot,limbtexture);
	bumbleRightThigh.setup(bumbleTorso.rightThighPivot,limbtexture);
	bumbleLeftLeg.setup(bumbleLeftThigh.leftLegPivot,limbtexture);
	bumbleRightLeg.setup(bumbleRightThigh.rightLegPivot,limbtexture);
	bumbleLeftFoot.setup(bumbleLeftLeg.leftFootPivot,bodytexture);
	bumbleRightFoot.setup(bumbleRightLeg.rightFootPivot,bodytexture);
	bumbleLeftShoulderWheel.setup(bumbleLeftShoulder.leftShoulderWheelPivot,tyretexture);
	bumbleRightShoulderWheel.setup(bumbleRightShoulder.rightShoulderWheelPivot,tyretexture);
	bumbleLeftArmWheel.setup(bumbleLeftArm.leftArmWheelPivot,tyretexture);
	bumbleRightArmWheel.setup(bumbleRightArm.rightArmWheelPivot,tyretexture);
}

void Transformer::render(){
	bumbleTorso.render();
	bumbleHead.render();
	bumbleLeftShoulder.render();
	bumbleRightShoulder.render();
	bumbleLeftArm.render();
	bumbleRightArm.render();
	bumbleLeftHand.render();
	bumbleRightHand.render();
	bumbleLeftThigh.render();
	bumbleRightThigh.render();
	bumbleLeftLeg.render();
	bumbleRightLeg.render();
	bumbleLeftFoot.render();
	bumbleRightFoot.render(); 
	bumbleLeftShoulderWheel.render();
	bumbleRightShoulderWheel.render();
	bumbleLeftArmWheel.render();
	bumbleRightArmWheel.render();
}

void setupLights(){
	add_light_source(vec3(1,0.71,0.29),vec3(25,30,-700),1,1);
	add_light_source(vec3(1,0.71,0.29),vec3(-25,30,-300),1,1);
	add_light_source(vec3(1,0.71,0.29),vec3(25,30,100),1,1);
	add_light_source(vec3(1,0.71,0.29),vec3(-25,30,400),1,1);
	add_light_source(vec3(1,0.71,0.29),vec3(25,30,700),1,1);
	add_light_source(vec3(1,0.71,0.29),vec3(-25,30,1100),1,1);
	add_light_source(vec3(1,0.71,0.29),vec3(25,30,1500),1,1);
	add_light_source(vec3(1,0.71,0.29),vec3(-25,30,1900),1,1);
	add_light_source(vec3(1,0.71,0.29),vec3(25,30,2300),1,1);
	
	add_light_source(vec3(1,0.71,0.29),vec3(400,30,100),1,1);
	add_light_source(vec3(1,0.71,0.29),vec3(800,30,100),1,1);
	add_light_source(vec3(1,0.71,0.29),vec3(1200,30,100),1,1);
	add_light_source(vec3(1,0.71,0.29),vec3(1600,30,100),1,1);
	add_light_source(vec3(1,0.71,0.29),vec3(-400,30,100),1,1);
	add_light_source(vec3(1,0.71,0.29),vec3(-800,30,100),1,1);
	add_light_source(vec3(1,0.71,0.29),vec3(-1200,30,100),1,1);
	add_light_source(vec3(1,0.71,0.29),vec3(-1600,30,100),1,1);
	
	add_light_source(vec3(1,0.71,0.29),vec3(400,30,800),1,1);
	add_light_source(vec3(1,0.71,0.29),vec3(800,30,800),1,1);
	add_light_source(vec3(1,0.71,0.29),vec3(-400,30,800),1,1);
	add_light_source(vec3(1,0.71,0.29),vec3(-800,30,800),1,1);
	
	add_light_source(vec3(1,0.71,0.29),vec3(800,30,400),1,1);
	add_light_source(vec3(1,0.71,0.29),vec3(800,30,1200),1,1);
	add_light_source(vec3(1,0.71,0.29),vec3(800,30,1600),1,1);
	
	add_light_source(vec3(1,0.71,0.29),vec3(-800,30,400),1,1);
	add_light_source(vec3(1,0.71,0.29),vec3(-800,30,1200),1,1);
	add_light_source(vec3(1,0.71,0.29),vec3(-800,30,1600),1,1);
	
	add_light_source(vec3(1,0.71,0.29),vec3(400,30,1700),1,1);
	add_light_source(vec3(1,0.71,0.29),vec3(800,30,1700),1,1);
	add_light_source(vec3(1,0.71,0.29),vec3(1200,30,1700),1,1);
	add_light_source(vec3(1,0.71,0.29),vec3(1600,30,1700),1,1);
	add_light_source(vec3(1,0.71,0.29),vec3(2000,30,1700),1,1);
	
	add_light_source(vec3(1,0.71,0.29),vec3(-400,30,1700),1,1);
	add_light_source(vec3(1,0.71,0.29),vec3(-800,30,1700),1,1);
	add_light_source(vec3(1,0.71,0.29),vec3(-1200,30,1700),1,1);
	add_light_source(vec3(1,0.71,0.29),vec3(-1600,30,1700),1,1);
	add_light_source(vec3(1,0.71,0.29),vec3(-2000,30,1700),1,1);

	vec3 carP = vec3(Globals::TransformerMatrix[3]);	

	add_light_source_full(vec3(0.83,0.92,1),carP+Globals::head1off,1,30,0.001,1);
	add_light_source_full(vec3(0.83,0.92,1),carP+Globals::head2off,1,30,0.001,1);
	
	light_setup();
}

void setupScene(){
	skyBox.setup();	
	setupLights();
	road1.setup(50,10,800,vec3(0,-30,0),road1t);
	road2.setup(800,10,50,vec3(0,-30,850),road2t);
	road3.setup(50,10,800,vec3(850,-30,900),road1t);
	road4.setup(800,10,50,vec3(850,-30,50),road2t);
	road5.setup(50,10,800,vec3(-850,-30,900),road1t);
	road6.setup(800,10,50,vec3(-850,-30,50),road2t);
	
	road7.setup(800,10,50,vec3(850,-30,1750),road2t);
	road8.setup(50,10,800,vec3(0,-30,1650),road1t);
	road9.setup(1600,10,50,vec3(600,-30,1750),road2t);
}

void renderScene(){
	skyBox.render();
}

void setupTransformer(){	
	glGenVertexArrays(1, &VertexArrayID);
	glBindVertexArray(VertexArrayID);

	t1.create(vec3(-20,15,0),bumbletexture, fullhandtexture);
	t2.create(vec3(15,15,0),bumbletexture2, fullhandtexture);

	transformersList.push_back(&t1);
	transformersList.push_back(&t2);

	Globals::activeTransformer = &t1;
	setupScene();
}

void renderTransformer(){

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );	

	Globals::updateMode();
	Globals::update_cameras();

	glUseProgram(Globals::ProgramId);

	computeMatricesFromInputs();	

	vector<Transformer*>::iterator transiterate = transformersList.begin();
	while(transiterate!=transformersList.end()){
		(**transiterate).render();	
		transiterate++;
	}

	road1.render();
	road2.render();
	road3.render();
	road4.render();
	road5.render();
	road6.render();
	road7.render();
	road8.render();
	road9.render();
	push_lights();

	glUseProgram(Globals::SkyboxProgramId);
	renderScene();
}

void animate(){
	keyframes = readCsv("part1.txt");

	int i=0;
	float nx,ny,nz;
	float px,py,pz;
	float lsx,lsy,lsz;
	float rsx,rsy,rsz;
	float ltx,lty,ltz;
	float lttx,ltty,lttz;
	float rtx,rty,rtz;
	float lax,lay,laz;
	float lswx,lswy,lswz;
	float rax,ray,raz;
	float rswx,rswy,rswz;
	float lwx,lwy,lwz;
	float lawx,lawy,lawz;
	float rwx,rwy,rwz;
	float rawx,rawy,rawz;
	float llx,lly,llz;
	float rlx,rly,rlz;
	float lfx,lfy,lfz;
	float rfx,rfy,rfz;
	float dirpx,dirpy,dirpz;
	float dirrx,dirry,dirrz;
	int car,headl;
	int turn;

	int first,second;
	for(first=0,second=1; second<keyframes.size(); first++,second++){
		double startTime = glfwGetTime();
		double currTime = glfwGetTime();
		
		float totalTime = keyframes[second].timestamp - keyframes[first].timestamp;

		float prevLoopTime = glfwGetTime();
		float currLoopTime = glfwGetTime();
		float deltaTime;
		
		printf("mode:%d\n",Globals::mode);
		Globals::headlights = keyframes[second].globalProp["headl"];
		Globals::streetlights = keyframes[second].globalProp["streetl"];
		Globals::mode = keyframes[second].globalProp["mode"];

		Globals::activeIndex = keyframes[second].globalProp["active"];
		Globals::activeTransformer = transformersList[Globals::activeIndex];
		Globals::currentMode = keyframes[second].globalProp["cammode"];

		float rota = 0;
		while((currTime - startTime) < totalTime){
			prevLoopTime = currLoopTime;
			currLoopTime = glfwGetTime();	

			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );	

			Globals::updateMode();
			Globals::update_cameras();

			glUseProgram(Globals::ProgramId);

			deltaTime = currLoopTime - prevLoopTime;
			for(int transindex=0; transindex<keyframes[first].transformerProp.size() ; transindex++){

				nx = keyframes[second].transformerProp[transindex]["nx"] - keyframes[first].transformerProp[transindex]["nx"];
				ny = keyframes[second].transformerProp[transindex]["ny"] - keyframes[first].transformerProp[transindex]["ny"];
				nz = keyframes[second].transformerProp[transindex]["nz"] - keyframes[first].transformerProp[transindex]["nz"];
	
				if(nx != 0)		
					(*transformersList[transindex]).bumbleTorso.neckPivot.rotateNeckPivot(nx*(deltaTime/totalTime),vec3(1,0,0),*(transformersList[transindex]));

				if(ny != 0)
					(*transformersList[transindex]).bumbleTorso.neckPivot.rotateNeckPivot(ny*(deltaTime/totalTime),vec3(0,1,0),*(transformersList[transindex]));

				if(nz != 0)
					(*transformersList[transindex]).bumbleTorso.neckPivot.rotateNeckPivot(nz*(deltaTime/totalTime),vec3(0,0,1),*(transformersList[transindex]));
				
				lsx = keyframes[second].transformerProp[transindex]["lsx"] - keyframes[first].transformerProp[transindex]["lsx"];
				lsy = keyframes[second].transformerProp[transindex]["lsy"] - keyframes[first].transformerProp[transindex]["lsy"];
				lsz = keyframes[second].transformerProp[transindex]["lsz"] - keyframes[first].transformerProp[transindex]["lsz"];
	
				if(lsx != 0)		
					(*transformersList[transindex]).bumbleTorso.leftShoulderPivot.rotateLeftShoulderPivot(lsx*(deltaTime/totalTime),vec3(1,0,0),*(transformersList[transindex]));

				if(lsy != 0 && Globals::mode!=1)
					(*transformersList[transindex]).bumbleTorso.leftShoulderPivot.rotateLeftShoulderPivot(lsy*(deltaTime/totalTime),vec3(0,1,0),*(transformersList[transindex]));

				if(lsz != 0)
					(*transformersList[transindex]).bumbleTorso.leftShoulderPivot.rotateLeftShoulderPivot(lsz*(deltaTime/totalTime),vec3(0,0,1),*(transformersList[transindex]));
				
				rsx = keyframes[second].transformerProp[transindex]["rsx"] - keyframes[first].transformerProp[transindex]["rsx"];
				rsy = keyframes[second].transformerProp[transindex]["rsy"] - keyframes[first].transformerProp[transindex]["rsy"];
				rsz = keyframes[second].transformerProp[transindex]["rsz"] - keyframes[first].transformerProp[transindex]["rsz"];
	
				if(rsx != 0)			
					(*transformersList[transindex]).bumbleTorso.rightShoulderPivot.rotateRightShoulderPivot(rsx*(deltaTime/totalTime),vec3(1,0,0),*(transformersList[transindex]));

				if(rsy != 0 && Globals::mode!=1)
					(*transformersList[transindex]).bumbleTorso.rightShoulderPivot.rotateRightShoulderPivot(rsy*(deltaTime/totalTime),vec3(0,1,0),*(transformersList[transindex]));

				if(rsz != 0)
					(*transformersList[transindex]).bumbleTorso.rightShoulderPivot.rotateRightShoulderPivot(rsz*(deltaTime/totalTime),vec3(0,0,1),*(transformersList[transindex]));
		
				ltx = keyframes[second].transformerProp[transindex]["ltx"] - keyframes[first].transformerProp[transindex]["ltx"];
				lty = keyframes[second].transformerProp[transindex]["lty"] - keyframes[first].transformerProp[transindex]["lty"];
				ltz = keyframes[second].transformerProp[transindex]["ltz"] - keyframes[first].transformerProp[transindex]["ltz"];
	
				if(ltx != 0)	
					(*transformersList[transindex]).bumbleTorso.leftThighPivot.rotateLeftThighPivot(ltx*(deltaTime/totalTime),vec3(1,0,0),true,(*transformersList[transindex]));

				if(lty != 0)
					(*transformersList[transindex]).bumbleTorso.leftThighPivot.rotateLeftThighPivot(lty*(deltaTime/totalTime),vec3(0,1,0),true,(*transformersList[transindex]));

				if(ltz != 0)
					(*transformersList[transindex]).bumbleTorso.leftThighPivot.rotateLeftThighPivot(ltz*(deltaTime/totalTime),vec3(0,0,1),true,(*transformersList[transindex]));
		
				lttx = keyframes[second].transformerProp[transindex]["lttx"] - keyframes[first].transformerProp[transindex]["lttx"];
				ltty = keyframes[second].transformerProp[transindex]["ltty"] - keyframes[first].transformerProp[transindex]["ltty"];
				lttz = keyframes[second].transformerProp[transindex]["lttz"] - keyframes[first].transformerProp[transindex]["lttz"];
				
				if(lttx != 0)
					(*transformersList[transindex]).bumbleTorso.leftThighTPivot.rotateLeftThighTPivot(lttx*(deltaTime/totalTime),vec3(1,0,0),(*transformersList[transindex]));
				if(ltty != 0)
					(*transformersList[transindex]).bumbleTorso.leftThighTPivot.rotateLeftThighTPivot(ltty*(deltaTime/totalTime),vec3(0,1,0),(*transformersList[transindex]));
				if(lttz != 0)
					(*transformersList[transindex]).bumbleTorso.leftThighTPivot.rotateLeftThighTPivot(lttz*(deltaTime/totalTime),vec3(0,0,1),(*transformersList[transindex]));
	
				rtx = keyframes[second].transformerProp[transindex]["rtx"] - keyframes[first].transformerProp[transindex]["rtx"];
				rty = keyframes[second].transformerProp[transindex]["rty"] - keyframes[first].transformerProp[transindex]["rty"];
				rtz = keyframes[second].transformerProp[transindex]["rtz"] - keyframes[first].transformerProp[transindex]["rtz"];
	
				if(rtx != 0)			
					(*transformersList[transindex]).bumbleTorso.rightThighPivot.rotateRightThighPivot(rtx*(deltaTime/totalTime),vec3(1,0,0),true,(*transformersList[transindex]));

				if(rty != 0)
					(*transformersList[transindex]).bumbleTorso.rightThighPivot.rotateRightThighPivot(rty*(deltaTime/totalTime),vec3(0,1,0),true,(*transformersList[transindex]));

				if(rtz != 0)
					(*transformersList[transindex]).bumbleTorso.rightThighPivot.rotateRightThighPivot(rtz*(deltaTime/totalTime),vec3(0,0,1),true,(*transformersList[transindex]));
			
				lax = keyframes[second].transformerProp[transindex]["lax"] - keyframes[first].transformerProp[transindex]["lax"];
				lay = keyframes[second].transformerProp[transindex]["lay"] - keyframes[first].transformerProp[transindex]["lay"];
				laz = keyframes[second].transformerProp[transindex]["laz"] - keyframes[first].transformerProp[transindex]["laz"];
	
				if(lax != 0)			
					(*transformersList[transindex]).bumbleLeftShoulder.leftArmPivot.rotateLeftArmPivot(lax*(deltaTime/totalTime),vec3(1,0,0),(*transformersList[transindex]));

				if(lay != 0)
					(*transformersList[transindex]).bumbleLeftShoulder.leftArmPivot.rotateLeftArmPivot(lay*(deltaTime/totalTime),vec3(0,1,0),(*transformersList[transindex]));

				if(laz != 0)
					(*transformersList[transindex]).bumbleLeftShoulder.leftArmPivot.rotateLeftArmPivot(laz*(deltaTime/totalTime),vec3(0,0,1),(*transformersList[transindex]));
				
				lswx = keyframes[second].transformerProp[transindex]["lswx"] - keyframes[first].transformerProp[transindex]["lswx"];
				lswy = keyframes[second].transformerProp[transindex]["lswy"] - keyframes[first].transformerProp[transindex]["lswy"];
				lswz = keyframes[second].transformerProp[transindex]["lswz"] - keyframes[first].transformerProp[transindex]["lswz"];
				
				if(lswx != 0)			
					(*transformersList[transindex]).bumbleLeftShoulder.leftShoulderWheelPivot.rotateLeftShoulderWheelPivot(lswx*(deltaTime/totalTime),vec3(1,0,0),(*transformersList[transindex]));

				if(lswy != 0)
					(*transformersList[transindex]).bumbleLeftShoulder.leftShoulderWheelPivot.rotateLeftShoulderWheelPivot(lswy*(deltaTime/totalTime),vec3(0,1,0),(*transformersList[transindex]));

				if(lswz != 0)
					(*transformersList[transindex]).bumbleLeftShoulder.leftShoulderWheelPivot.rotateLeftShoulderWheelPivot(lswz*(deltaTime/totalTime),vec3(0,0,1),(*transformersList[transindex]));
				
				rax = keyframes[second].transformerProp[transindex]["rax"] - keyframes[first].transformerProp[transindex]["rax"];
				ray = keyframes[second].transformerProp[transindex]["ray"] - keyframes[first].transformerProp[transindex]["ray"];
				raz = keyframes[second].transformerProp[transindex]["raz"] - keyframes[first].transformerProp[transindex]["raz"];
			
				if(lswx != 0)			
				(*transformersList[transindex]).bumbleRightShoulder.rightArmPivot.rotateRightArmPivot(rax*(deltaTime/totalTime),vec3(1,0,0),(*transformersList[transindex]));
				if(lswy != 0)			
				(*transformersList[transindex]).bumbleRightShoulder.rightArmPivot.rotateRightArmPivot(ray*(deltaTime/totalTime),vec3(0,1,0),(*transformersList[transindex]));
				if(lswz != 0)			
				(*transformersList[transindex]).bumbleRightShoulder.rightArmPivot.rotateRightArmPivot(raz*(deltaTime/totalTime),vec3(0,0,1),(*transformersList[transindex]));
				
				rswx = keyframes[second].transformerProp[transindex]["rswx"] - keyframes[first].transformerProp[transindex]["rswx"];
				rswy = keyframes[second].transformerProp[transindex]["rswy"] - keyframes[first].transformerProp[transindex]["rswy"];
				rswz = keyframes[second].transformerProp[transindex]["rswz"] - keyframes[first].transformerProp[transindex]["rswz"];
				
				if(rswx != 0)			
				(*transformersList[transindex]).bumbleRightShoulder.rightShoulderWheelPivot.rotateRightShoulderWheelPivot(rswx*(deltaTime/totalTime),vec3(1,0,0),(*transformersList[transindex]));
				if(rswy != 0)			
				(*transformersList[transindex]).bumbleRightShoulder.rightShoulderWheelPivot.rotateRightShoulderWheelPivot(rswy*(deltaTime/totalTime),vec3(0,1,0),(*transformersList[transindex]));
				if(rswz != 0)			
				(*transformersList[transindex]).bumbleRightShoulder.rightShoulderWheelPivot.rotateRightShoulderWheelPivot(rswz*(deltaTime/totalTime),vec3(0,0,1),(*transformersList[transindex]));
				
				lwx = keyframes[second].transformerProp[transindex]["lwx"] - keyframes[first].transformerProp[transindex]["lwx"];
				lwy = keyframes[second].transformerProp[transindex]["lwy"] - keyframes[first].transformerProp[transindex]["lwy"];
				lwz = keyframes[second].transformerProp[transindex]["lwz"] - keyframes[first].transformerProp[transindex]["lwz"];
			
				if(lwx!=0)	
				(*transformersList[transindex]).bumbleLeftArm.leftWristPivot.rotateLeftWristPivot(lwx*(deltaTime/totalTime),vec3(1,0,0),(*transformersList[transindex]));
				if(lwy!=0)	
				(*transformersList[transindex]).bumbleLeftArm.leftWristPivot.rotateLeftWristPivot(lwy*(deltaTime/totalTime),vec3(0,1,0),(*transformersList[transindex]));
				if(lwz!=0)	
				(*transformersList[transindex]).bumbleLeftArm.leftWristPivot.rotateLeftWristPivot(lwz*(deltaTime/totalTime),vec3(0,0,1),(*transformersList[transindex]));
				
				lawx = keyframes[second].transformerProp[transindex]["lawx"] - keyframes[first].transformerProp[transindex]["lawx"];
				lawy = keyframes[second].transformerProp[transindex]["lawy"] - keyframes[first].transformerProp[transindex]["lawy"];
				lawz = keyframes[second].transformerProp[transindex]["lawz"] - keyframes[first].transformerProp[transindex]["lawz"];
				if(lawx!=0)	
				(*transformersList[transindex]).bumbleLeftArm.leftArmWheelPivot.rotateLeftArmWheelPivot(lawx*(deltaTime/totalTime),vec3(1,0,0),(*transformersList[transindex]));
				if(lawy!=0)	
				(*transformersList[transindex]).bumbleLeftArm.leftArmWheelPivot.rotateLeftArmWheelPivot(lawy*(deltaTime/totalTime),vec3(0,1,0),(*transformersList[transindex]));
				if(lawz!=0)	
				(*transformersList[transindex]).bumbleLeftArm.leftArmWheelPivot.rotateLeftArmWheelPivot(lawz*(deltaTime/totalTime),vec3(0,0,1),(*transformersList[transindex]));
				
				rwx = keyframes[second].transformerProp[transindex]["rwx"] - keyframes[first].transformerProp[transindex]["rwx"];
				rwy = keyframes[second].transformerProp[transindex]["rwy"] - keyframes[first].transformerProp[transindex]["rwy"];
				rwz = keyframes[second].transformerProp[transindex]["rwz"] - keyframes[first].transformerProp[transindex]["rwz"];
			
				if(rwx!=0)	
				(*transformersList[transindex]).bumbleRightArm.rightWristPivot.rotateRightWristPivot(rwx*(deltaTime/totalTime),vec3(1,0,0),(*transformersList[transindex]));
				if(rwy!=0)	
				(*transformersList[transindex]).bumbleRightArm.rightWristPivot.rotateRightWristPivot(rwy*(deltaTime/totalTime),vec3(0,1,0),(*transformersList[transindex]));
				if(rwz!=0)	
				(*transformersList[transindex]).bumbleRightArm.rightWristPivot.rotateRightWristPivot(rwz*(deltaTime/totalTime),vec3(0,0,1),(*transformersList[transindex]));
				
				rawx = keyframes[second].transformerProp[transindex]["rawx"] - keyframes[first].transformerProp[transindex]["rawx"];
				rawy = keyframes[second].transformerProp[transindex]["rawy"] - keyframes[first].transformerProp[transindex]["rawy"];
				rawz = keyframes[second].transformerProp[transindex]["rawz"] - keyframes[first].transformerProp[transindex]["rawz"];
				
				if(rawx!=0)	
				(*transformersList[transindex]).bumbleRightArm.rightArmWheelPivot.rotateRightArmWheelPivot(rawx*(deltaTime/totalTime),vec3(1,0,0),(*transformersList[transindex]));
				if(rawy!=0)	
				(*transformersList[transindex]).bumbleRightArm.rightArmWheelPivot.rotateRightArmWheelPivot(rawy*(deltaTime/totalTime),vec3(0,1,0),(*transformersList[transindex]));
				if(rawz!=0)	
				(*transformersList[transindex]).bumbleRightArm.rightArmWheelPivot.rotateRightArmWheelPivot(rawz*(deltaTime/totalTime),vec3(0,0,1),(*transformersList[transindex]));
				
				llx = keyframes[second].transformerProp[transindex]["llx"] - keyframes[first].transformerProp[transindex]["llx"];
				lly = keyframes[second].transformerProp[transindex]["lly"] - keyframes[first].transformerProp[transindex]["lly"];
				llz = keyframes[second].transformerProp[transindex]["llz"] - keyframes[first].transformerProp[transindex]["llz"];
				
				if(llx!=0 && Globals::mode!=1)	
				(*transformersList[transindex]).bumbleLeftThigh.leftLegPivot.rotateLeftLegPivot(llx*(deltaTime/totalTime),vec3(1,0,0),(*transformersList[transindex]));
				if(lly!=0)	
				(*transformersList[transindex]).bumbleLeftThigh.leftLegPivot.rotateLeftLegPivot(lly*(deltaTime/totalTime),vec3(0,1,0),(*transformersList[transindex]));
				if(llz!=0)	
				(*transformersList[transindex]).bumbleLeftThigh.leftLegPivot.rotateLeftLegPivot(llz*(deltaTime/totalTime),vec3(0,0,1),(*transformersList[transindex]));
					
				rlx = keyframes[second].transformerProp[transindex]["rlx"] - keyframes[first].transformerProp[transindex]["rlx"];
				rly = keyframes[second].transformerProp[transindex]["rly"] - keyframes[first].transformerProp[transindex]["rly"];
				rlz = keyframes[second].transformerProp[transindex]["rlz"] - keyframes[first].transformerProp[transindex]["rlz"];
			
				if(rlx!=0 && Globals::mode!=1)	
				(*transformersList[transindex]).bumbleRightThigh.rightLegPivot.rotateRightLegPivot(rlx*(deltaTime/totalTime),vec3(1,0,0),(*transformersList[transindex]));
				if(rly!=0)	
				(*transformersList[transindex]).bumbleRightThigh.rightLegPivot.rotateRightLegPivot(rly*(deltaTime/totalTime),vec3(0,1,0),(*transformersList[transindex]));
				if(rlz!=0)	
				(*transformersList[transindex]).bumbleRightThigh.rightLegPivot.rotateRightLegPivot(rlz*(deltaTime/totalTime),vec3(0,0,1),(*transformersList[transindex]));
				
				lfx = keyframes[second].transformerProp[transindex]["lfx"] - keyframes[first].transformerProp[transindex]["lfx"];
				lfy = keyframes[second].transformerProp[transindex]["lfy"] - keyframes[first].transformerProp[transindex]["lfy"];
				lfz = keyframes[second].transformerProp[transindex]["lfz"] - keyframes[first].transformerProp[transindex]["lfz"];
			
				if(lfx!=0)	
				(*transformersList[transindex]).bumbleLeftLeg.leftFootPivot.rotateLeftFootPivot(lfx*(deltaTime/totalTime),vec3(1,0,0),(*transformersList[transindex]));
				if(lfy!=0)	
				(*transformersList[transindex]).bumbleLeftLeg.leftFootPivot.rotateLeftFootPivot(lfy*(deltaTime/totalTime),vec3(0,1,0),(*transformersList[transindex]));
				if(lfz!=0)	
				(*transformersList[transindex]).bumbleLeftLeg.leftFootPivot.rotateLeftFootPivot(lfz*(deltaTime/totalTime),vec3(0,0,1),(*transformersList[transindex]));
				
				rfx = keyframes[second].transformerProp[transindex]["rfx"] - keyframes[first].transformerProp[transindex]["rfx"];
				rfy = keyframes[second].transformerProp[transindex]["rfy"] - keyframes[first].transformerProp[transindex]["rfy"];
				rfz = keyframes[second].transformerProp[transindex]["rfz"] - keyframes[first].transformerProp[transindex]["rfz"];
			
				if(rfx!=0)	
				(*transformersList[transindex]).bumbleRightLeg.rightFootPivot.rotateRightFootPivot(rfx*(deltaTime/totalTime),vec3(1,0,0),(*transformersList[transindex]));
				if(rfy!=0)	
				(*transformersList[transindex]).bumbleRightLeg.rightFootPivot.rotateRightFootPivot(rfy*(deltaTime/totalTime),vec3(0,1,0),(*transformersList[transindex]));
				if(rfz!=0)	
				(*transformersList[transindex]).bumbleRightLeg.rightFootPivot.rotateRightFootPivot(rfz*(deltaTime/totalTime),vec3(0,0,1),(*transformersList[transindex]));
				
				px = keyframes[second].transformerProp[transindex]["px"] - keyframes[first].transformerProp[transindex]["px"];
				py = keyframes[second].transformerProp[transindex]["py"] - keyframes[first].transformerProp[transindex]["py"];
				pz = keyframes[second].transformerProp[transindex]["pz"] - keyframes[first].transformerProp[transindex]["pz"];
	
				if(px!=0)
					(*transformersList[transindex]).bumbleTorso.translate(vec3(px*(deltaTime/totalTime),0,0),*(transformersList[transindex]));
				if(py!=0){
					if(Globals::mode!=1)
						(*transformersList[transindex]).bumbleTorso.translate(vec3(0,py*(deltaTime/totalTime),0),*(transformersList[transindex]));
					else
						(*transformersList[transindex]).bumbleTorso.translate(vec3(0,0,-py*(deltaTime/totalTime)),*(transformersList[transindex]));
				}
				if(pz!=0){
					if(Globals::mode!=1)
						(*transformersList[transindex]).bumbleTorso.translate(vec3(0,0,pz*(deltaTime/totalTime)),*(transformersList[transindex]));
					else
						(*transformersList[transindex]).bumbleTorso.translate(vec3(0,pz*(deltaTime/totalTime),0),*(transformersList[transindex]));
				}

				turn = keyframes[second].transformerProp[transindex]["turn"] - keyframes[first].transformerProp[transindex]["turn"];
				if(turn!=0)
					(*transformersList[transindex]).bumbleTorso.rotate(turn, *(transformersList[transindex]));
					

			}
				
			dirpx = keyframes[second].globalProp["dirpx"] - keyframes[first].globalProp["dirpx"] ;
			if(dirpx!=0)
				Globals::dir_cam_translate(vec3(dirpx*(deltaTime/totalTime),0,0));
			
			dirpy = keyframes[second].globalProp["dirpy"] - keyframes[first].globalProp["dirpy"] ;
			if(dirpy!=0)
				Globals::dir_cam_translate(vec3(0,dirpy*(deltaTime/totalTime),0));
			
			dirpz = keyframes[second].globalProp["dirpz"] - keyframes[first].globalProp["dirpz"] ;
			if(dirpz!=0)
				Globals::dir_cam_translate(vec3(0,0,dirpz*(deltaTime/totalTime)));
			
			dirrx = keyframes[second].globalProp["dirrx"] - keyframes[first].globalProp["dirrx"] ;
			if(dirrx!=0)
				Globals::dir_cam_rotate(dirrx*(deltaTime/totalTime),vec3(1,0,0));
			
			dirry = keyframes[second].globalProp["dirry"] - keyframes[first].globalProp["dirry"] ;
			if(dirry!=0)
				Globals::dir_cam_rotate(dirry*(deltaTime/totalTime),vec3(0,1,0));
			
			dirrz = keyframes[second].globalProp["dirrz"] - keyframes[first].globalProp["dirrz"] ;
			if(dirrz!=0)
				Globals::dir_cam_rotate(dirry*(deltaTime/totalTime),vec3(0,0,1));
		
			vector<Transformer*>::iterator transiterate = transformersList.begin();
			while(transiterate!=transformersList.end()){
				(**transiterate).render();	
				transiterate++;
			}

			road1.render();
			road2.render();
			road3.render();
			road4.render();
			road5.render();
			road6.render();
			road7.render();
			road8.render();
			road9.render();
			push_lights();

			glUseProgram(Globals::SkyboxProgramId);
			renderScene();
			
			glfwSwapBuffers(mainwindow);
			glfwPollEvents();

			currTime = glfwGetTime();
		}

	}	
}
