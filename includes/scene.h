#ifndef SCENE_H
#define SCENE_H
#include<stdio.h> 
#include"models.h"
#include"parts.h"
#include<vector>

extern const char* skytexture;
extern const char* roadtexture;

using namespace std;

class Skybox{
	public:
		cuboid skybox;
		float width=1000,height=1000,depth=1000;
		vec3 position = vec3(0,0,0);
		
		void setup();

		void render();
	
		void translate(vec3 trans);		
};

class Road : public BodyPart{
	public:
		cuboid road;

		void setup(float width,float height,float depth,vec3 position, const char* texture);

		void render();
};

#endif
