#ifndef PARTS_H
#define PARTS_H
#include<stdio.h> 
#include"models.h"
#include<vector>

using namespace std;

extern const char* secondTexture;
extern const char* eyetexture;
extern const char* pivottexture;
extern const char* fullhandtexture;
extern const char* fulllegtexture;
extern const char* bumbletexture;
extern const char* tyretexture;

class Transformer;

class BodyPart{
	public:
		vector<cuboid> components;
		vector<cylinder> cylcomponents;
		vec3 offsetFromParent;

		void commonRender();
		void commonCylinderRender();
		void readjust(mat4 parentMatrix);
};

class SinglePivotBase : public BodyPart{
	public:
		class rotation{
			public:
				float angle;
				vec3 axis;
				bool leg;
				rotation(float a,vec3 ax){
					angle = a;
					axis = ax;
				}
				
				rotation(float a,vec3 ax,bool l){
					angle = a;
					axis = ax;
					leg = l;
				}
		};

		vector<rotation> rotations;

		cuboid pivot;
		vec3 offsetFromParent;
		
		void setup(vec3 parentPos,vec3 offset,const char* texturef);

		void render();

		mat4 getModelMatrix();
		vec3 getRotationAngle();		
		vec3 getRotationAnglet();		

		void setModelMatrix(mat4 givenMatrix);

		float rotX = 0.0;
		float rotY = 0.0;
		float rotZ = 0.0;

		float rotXt = 0.0;
		float rotYt = 0.0;
		float rotZt = 0.0;
};

class SinglePivot : public SinglePivotBase{
		public:
			void rotateNeckPivot(float angle,vec3 axis, Transformer &t);
			void readjustNeckPivot(mat4 parentMatrix, Transformer &t);

			void rotateLeftShoulderPivot(float angle,vec3 axis, Transformer &t);
			void readjustLeftShoulderPivot(mat4 parentMatrix, Transformer &t);

			void rotateRightShoulderPivot(float angle,vec3 axis, Transformer &t);
			void readjustRightShoulderPivot(mat4 parentMatrix, Transformer &t);

			void rotateLeftArmPivot(float angle,vec3 axis, Transformer &t);
			void readjustLeftArmPivot(mat4 parentMatrix, Transformer &t);

			void rotateRightArmPivot(float angle,vec3 axis, Transformer &t);
			void readjustRightArmPivot(mat4 parentMatrix, Transformer &t);

			void rotateLeftWristPivot(float angle,vec3 axis, Transformer &t);
			void readjustLeftWristPivot(mat4 parentMatrix, Transformer &t);

			void rotateRightWristPivot(float angle,vec3 axis, Transformer &t);
			void readjustRightWristPivot(mat4 parentMatrix, Transformer &t);

			void rotateLeftThighPivot(float angle,vec3 axis,bool leg, Transformer &t);
			void readjustLeftThighPivot(mat4 parentMatrix, Transformer &t);

			void rotateLeftThighTPivot(float angle,vec3 axis,Transformer &t);
			void readjustLeftThighTPivot(mat4 parentMatrix, Transformer &t);

			void rotateRightThighPivot(float angle,vec3 axis,bool leg, Transformer &t);
			void readjustRightThighPivot(mat4 parentMatrix, Transformer &t);

			void rotateLeftLegPivot(float angle,vec3 axis, Transformer &t);
			void readjustLeftLegPivot(mat4 parentMatrix, Transformer &t);

			void rotateRightLegPivot(float angle,vec3 axis, Transformer &t);
			void readjustRightLegPivot(mat4 parentMatrix, Transformer &t);

			void rotateLeftFootPivot(float angle,vec3 axis, Transformer &t);
			void readjustLeftFootPivot(mat4 parentMatrix, Transformer &t);

			void rotateRightFootPivot(float angle,vec3 axis, Transformer &t);
			void readjustRightFootPivot(mat4 parentMatrix, Transformer &t);
			
			void rotateLeftShoulderWheelPivot(float angle,vec3 axis, Transformer &t);
			void readjustLeftShoulderWheelPivot(mat4 parentMatrix, Transformer &t);
			
			void rotateRightShoulderWheelPivot(float angle,vec3 axis, Transformer &t);
			void readjustRightShoulderWheelPivot(mat4 parentMatrix, Transformer &t);
			
			void rotateLeftArmWheelPivot(float angle,vec3 axis, Transformer &t);
			void readjustLeftArmWheelPivot(mat4 parentMatrix, Transformer &t);
			
			void rotateRightArmWheelPivot(float angle,vec3 axis, Transformer &t);
			void readjustRightArmWheelPivot(mat4 parentMatrix, Transformer &t);
};

class Torso : public BodyPart{
	public:
		cuboid torso;
		float width=6,height=8,depth=3;
		vec3 position = vec3(0,0,0);
		vec3 offsetFromParent = vec3((width/2.0f),height+1,0);	
		
		SinglePivot neckPivot;
		SinglePivot leftShoulderPivot;
		SinglePivot rightShoulderPivot;
		SinglePivot leftThighPivot;
		SinglePivot leftThighTPivot;
		SinglePivot rightThighPivot;

		void setup(vec3 position,const char *texture);

		void render();
		
		void readjust(mat4 parentMatrix, Transformer &t);

		void translate(vec3 trans, Transformer &t);
		void rotate(float angle, Transformer &t);
};

class Head : public BodyPart{
	public:
		SinglePivot parent;
		mat4 parentMatrix;
		float width = 3, height = 3, depth = 2;
		cuboid head;
		vec3 position = vec3(0,0,0);
		vec3 offsetFromParent = vec3(0,4,0);	
	
		void setup(SinglePivot parent, const char *texture);

		void render();

		void readjust(mat4 parentMatrix);
};

class LeftShoulder : public BodyPart{
	public:
		SinglePivot parent;
		mat4 parentMatrix;
		float width = 2, height = 3, depth = 2;
		cuboid leftshoulder;
		vec3 position = vec3(0,0,0);
		vec3 offsetFromParent = vec3(-width-1,0,0);
		SinglePivot leftArmPivot;
		SinglePivot leftShoulderWheelPivot;

		void setup(SinglePivot parent, const char *texture);

		void render();

		void readjust(mat4 parentMatrix, Transformer &t);
};

class RightShoulder : public BodyPart{
	public:
		SinglePivot parent;
		mat4 parentMatrix;
		float width = 2, height = 3, depth = 2;
		cuboid rightshoulder;	
		vec3 position = vec3(0,0,0);
		vec3 offsetFromParent = vec3(width+1,0.0,0.0);	
		SinglePivot rightArmPivot;
		SinglePivot rightShoulderWheelPivot;
		
		void setup(SinglePivot parent, const char *texture);

		void render();
		
		void readjust(mat4 parentMatrix, Transformer &t);
};

class LeftArm : public BodyPart{
	public:
		SinglePivot parent;
		mat4 parentMatrix;
		float width = 2, height = 5, depth = 2;
		cuboid leftarm;
		vec3 position = vec3(0,0,0);
		vec3 offsetFromParent = vec3(0,-6,0);
		SinglePivot leftWristPivot;
		SinglePivot leftArmWheelPivot;
		
		void setup(SinglePivot parent, const char *texture);

		void render();

		void readjust(mat4 parentMatrix, Transformer &t);

};

class LeftShoulderWheel : public BodyPart{
	public:
		SinglePivot parent;
		mat4 parentMatrix;
		float radius = 5, height = 2;
		cylinder leftshoulderwheel;
		vec3 position = vec3(0,0,0);
		vec3 offsetFromParent = vec3(0,0,-2);
		
		void setup(SinglePivot parent, const char *texture);

		void render();

		void readjust(mat4 parentMatrix);

};

class RightShoulderWheel : public BodyPart{
	public:
		SinglePivot parent;
		mat4 parentMatrix;
		float radius = 5, height = 2;
		cylinder rightshoulderwheel;
		vec3 position = vec3(0,0,0);
		vec3 offsetFromParent = vec3(0,0,-2);
		
		void setup(SinglePivot parent, const char *texture);

		void render();

		void readjust(mat4 parentMatrix);

};

class LeftArmWheel : public BodyPart{
	public:
		SinglePivot parent;
		mat4 parentMatrix;
		float radius = 5, height = 2;
		cylinder leftarmwheel;
		vec3 position = vec3(0,0,0);
		vec3 offsetFromParent = vec3(0,0,-2);
		
		void setup(SinglePivot parent, const char *texture);

		void render();

		void readjust(mat4 parentMatrix);

};

class RightArmWheel : public BodyPart{
	public:
		SinglePivot parent;
		mat4 parentMatrix;
		float radius = 5, height = 2;
		cylinder rightarmwheel;
		vec3 position = vec3(0,0,0);
		vec3 offsetFromParent = vec3(0,0,-2);
		
		void setup(SinglePivot parent, const char *texture);

		void render();

		void readjust(mat4 parentMatrix);

};

class RightArm : public BodyPart{
	public:
		SinglePivot parent;
		mat4 parentMatrix;
		float width = 2, height = 5, depth = 2;
		cuboid rightarm;
		vec3 position = vec3(0,0,0);	
		vec3 offsetFromParent = vec3(0,-6,0);
		SinglePivot rightWristPivot;
		SinglePivot rightArmWheelPivot;

		void setup(SinglePivot p, const char *texture);

		void render();
		
		void readjust(mat4 parentMatrix, Transformer &t);
};

class LeftHand : public BodyPart{
	public:
		SinglePivot parent;
		mat4 parentMatrix;
		float width = 2, height = 2, depth = 1;
		cuboid lefthand;
		vec3 position = vec3(0,0,0);
		vec3 offsetFromParent = vec3(0,-3,0);

		void setup(SinglePivot parent, const char *texture);

		void render();
		
		void readjust(mat4 parentMatrix);
};

class RightHand : public BodyPart{
	public:
		SinglePivot parent;
		mat4 parentMatrix;
		float width = 2, height = 2, depth = 1;
		cuboid righthand;
		vec3 position = vec3(0,0,0);
		vec3 offsetFromParent = vec3(0,-3,0);
		
		void setup(SinglePivot parent, const char *texture);

		void render();
		
		void readjust(mat4 parentMatrix);
};

class LeftThigh : public BodyPart{
	public:
		SinglePivot parent;
		mat4 parentMatrix;
		float width = 2, height = 4, depth = 2;
		cuboid leftthigh;
		vec3 position = vec3(0,0,0);
		vec3 offsetFromParent = vec3(0,-5,0);
		SinglePivot leftLegPivot;

		void setup(SinglePivot parent, const char *texture);

		void render();

		void readjust(mat4 parentMatrix, Transformer &t);
};

class RightThigh : public BodyPart{
	public:
		SinglePivot parent;
		mat4 parentMatrix;
		float width = 2, height = 4, depth = 2;
		cuboid rightthigh;
		vec3 position = vec3(0,0,0);
		vec3 offsetFromParent = vec3(0,-5,0);
		SinglePivot rightLegPivot;

		void setup(SinglePivot parent, const char *texture);

		void render();
		
		void readjust(mat4 parentMatrix, Transformer &t);
};

class LeftLeg : public BodyPart{
	public:
		SinglePivot parent;
		mat4 parentMatrix;
		float width = 2, height = 5, depth = 2;
		cuboid leftleg;
		vec3 position = vec3(0,0,0);
		vec3 offsetFromParent = vec3(0,-6,0);
		SinglePivot leftFootPivot;

		void setup(SinglePivot parent, const char *texture);

		void render();
		
		void readjust(mat4 parentMatrix, Transformer &t);
};

class RightLeg : public BodyPart{
	public:
		SinglePivot parent;
		mat4 parentMatrix;
		float width = 2, height = 5, depth = 2;
		cuboid rightleg;
		vec3 position = vec3(0,0,0);
		vec3 offsetFromParent = vec3(0,-6,0);
		SinglePivot rightFootPivot;

		void setup(SinglePivot parent, const char *texture);

		void render();
		
		void readjust(mat4 parentMatrix, Transformer &t);
};

class LeftFoot : public BodyPart{
	public:
		SinglePivot parent;
		mat4 parentMatrix;
		float width = 2, height = 1, depth = 3;
		cuboid leftfoot;
		vec3 position = vec3(0,0,0);
		vec3 offsetFromParent = vec3(0,-3,0);

		void setup(SinglePivot parent, const char *texture);

		void render();
		
		void readjust(mat4 parentMatrix);
};

class RightFoot : public BodyPart{
	public:
		SinglePivot parent;
		mat4 parentMatrix;
		float width = 2, height = 1, depth = 3;
		cuboid rightfoot;
		vec3 position = vec3(0,0,0);
		vec3 offsetFromParent = vec3(0,-3,0);

		void setup(SinglePivot parent, const char *texture);

		void render();
		
		void readjust(mat4 parentMatrix);
};
#endif
