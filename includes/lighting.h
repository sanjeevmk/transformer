#ifndef LIGHTING_H
#define LIGHTING_H
#include <GL/glew.h>
#define GLM_FORCE_RADIANS
#include "../glm/glm/gtc/matrix_transform.hpp"
#include "../glm/glm/glm.hpp"
#include "globals.h"

using namespace std;
using namespace glm;

struct light_source{
	GLfloat Ld[4], Ls[4], location[4];
	int type;
	float radii, specular_exponent;
};

struct light_source_ids{
	GLuint Ld, Ls, location;
	GLuint type;
	GLuint radii, specular_exponent;
};

static GLfloat head1pos[4];
static GLfloat head2pos[4];

static light_source light_sources[50];
static light_source_ids light_sources_ids[50];
static int count = 0;

bool add_light_source(vec3 _Ld, vec3 _location, unsigned int _type, int fixed);
bool add_light_source_full(vec3 _Ld, vec3 _location, unsigned int _type, float _radii, float _specular_exponent, int fixed);

void light_setup();
void push_lights();

static GLuint lightcountId;

#endif
