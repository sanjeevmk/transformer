#ifndef LOADBMP_H
#define LOADBMP_H
#include<vector>

using namespace std;

class imageData {
	public:
		unsigned int width;
		unsigned int height;
		unsigned char * data;
};

vector<imageData> loadCubeMapImage(vector<const char*> paths);
#endif
