#ifndef GLOBALS_H
#define GLOBALS_H
#include<GL/glew.h>
#define GLM_FORCE_RADIANS
#include"../glm/glm/glm.hpp"
#include "../glm/glm/gtc/matrix_transform.hpp"
#include "../glm/glm/gtx/rotate_vector.hpp"
#include<cmath>
#include<stdio.h>

using namespace std;
using namespace glm;

class Transformer;

#define todegrees(x) (x*(180.0/M_PI))
#define square(x) ((x)*(x))
class Globals{
	public:
		static GLuint ProgramId, MatrixId, SkyboxProgramId, SkyboxMatrixId;
		static GLuint view_mat_id, model_mat_id, proj_mat_id, light_id;
		static mat4 Projection,View;
		static mat4 fpView,chaseView,globalView,directorView;
		static int currentMode;
		static int activeIndex;

		static Transformer *activeTransformer;
		static mat4 TransformerMatrix;
		static vec3 carPosition;
		static float angle, dirX,dirY,dirZ;

		static vec3 dir_camera_position;
		static vec3 dir_look;
		static vec3 chaseOffset;
		static vec3 fpOffset;
		static vec3 fpLookAt;
		static vec3 head1off;
		static vec3 head2off;

		static int mode, streetlights, headlights;

		static void set_global_camera(vec3 camera_position, vec3 look, vec3 dir_cam_position, vec3 dir_l){
			globalView = lookAt(  camera_position,
					look,
					vec3(0,1,0) );

			dir_camera_position = dir_cam_position;
			dir_look = dir_l;
			activeIndex = 0;

			directorView = lookAt ( dir_camera_position,
						dir_look,
						vec3(0,1,0) );
		}
	
		static void dir_cam_rotate(float angle, vec3 axis){
			vec3 oldPos = dir_camera_position;
			vec3 oldLookAt = dir_look;

			dir_cam_translate(vec3(-dir_camera_position.x,-dir_camera_position.y,-dir_camera_position.z));
			dir_look = glm::rotate(dir_look - dir_camera_position, angle, axis);	
			dir_cam_translate(vec3(oldPos.x,oldPos.y,oldPos.z));
			if (axis == vec3(1,0,0))
				dirX += angle;
			if (axis == vec3(0,1,0))
				dirY += angle;
			if (axis == vec3(0,0,1))
				dirZ += angle;
			
		}

		static void dir_cam_translate(vec3 trans){
			dir_camera_position.x+=trans.x;
			dir_camera_position.y+=trans.y;
			dir_camera_position.z+=trans.z;
			dir_look.x+=trans.x;
			dir_look.y+=trans.y;
			dir_look.z+=trans.z;
		}
	
		static void init_cameras(){
			angle = 0;
			carPosition = vec3(TransformerMatrix[3]);
			chaseOffset = vec3(0,20,-80);
			fpOffset = vec3(0,9,0);
			fpLookAt = vec3(0,0,10);
			head1off = vec3(15,-5,90);
			head2off = vec3(-15,-5,90);

			fpView = lookAt( vec3(carPosition+fpOffset),
					vec3(carPosition+fpOffset+fpLookAt),
					vec3(0,1,0) );
			
			chaseView = lookAt( vec3(carPosition+chaseOffset),
					carPosition,
					vec3(0,1,0) );
		}

		static void getOrientation(){
			chaseOffset = vec3(0,20,-80);
			chaseOffset = glm::rotate((carPosition+chaseOffset)-(carPosition),-angle,vec3(0,1,0));
			
			fpOffset = vec3(0,9,0);
			fpOffset = glm::rotate(fpOffset,-angle,vec3(0,1,0));
			
			fpLookAt = vec3(0,0,10);
			fpLookAt = glm::rotate(fpLookAt,-angle,vec3(0,1,0)); 

			head1off = vec3(15,-5,90);
			head1off = glm::rotate(head1off,-angle,vec3(0,1,0));
			
			head2off = vec3(-15,-5,90);
			head2off = glm::rotate(head2off,-angle,vec3(0,1,0));
		}

		static void update_cameras(){
			getOrientation();
			vec3 carPosition = vec3(TransformerMatrix[3]);

			fpView = lookAt( vec3(carPosition+fpOffset),
					vec3(carPosition+fpOffset+fpLookAt),
					vec3(0,1,0) );
			
			chaseView = lookAt( vec3(carPosition+chaseOffset),
					carPosition,
					vec3(0,1,0) );
		
			directorView = lookAt ( dir_camera_position,
						dir_look,
						vec3(0,1,0) );
		}

		static void updateMode(){
			if(currentMode == 0)
				View = globalView;
			if(currentMode == 1)
				View = fpView;
			if(currentMode == 2)
				View = chaseView;
			if(currentMode == 3)
				View = directorView;
		}
};

#endif
