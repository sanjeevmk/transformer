#ifndef TRANSFORMER_H
#define TRANSFORMER_H
#include"parts.h"

class Transformer{
	public:
		Torso bumbleTorso;
		Head bumbleHead;
		LeftShoulder bumbleLeftShoulder;
		RightShoulder bumbleRightShoulder;
		LeftArm bumbleLeftArm;
		RightArm bumbleRightArm;
		LeftHand bumbleLeftHand;
		RightHand bumbleRightHand;
		LeftThigh bumbleLeftThigh;
		RightThigh bumbleRightThigh;
		LeftLeg bumbleLeftLeg;
		RightLeg bumbleRightLeg;
		LeftFoot bumbleLeftFoot;
		RightFoot bumbleRightFoot;
		LeftShoulderWheel bumbleLeftShoulderWheel;
		RightShoulderWheel bumbleRightShoulderWheel;
		LeftArmWheel bumbleLeftArmWheel;
		RightArmWheel bumbleRightArmWheel;

		bool car = false;

		float turnAngle = 0;

		void create(vec3 position,const char *bodytexture, const char *limbtexture);
		void render();
};
#endif
