#ifndef MODELS_H
#define MODELS_H
#include <stdio.h>
#include <GL/glew.h>
#define GLM_FORCE_RADIANS
#include "../glm/glm/glm.hpp"
#include "../glm/glm/gtc/matrix_transform.hpp"
#include "../glm/glm/gtc/quaternion.hpp"
#include "../glm/glm/gtx/quaternion.hpp"
#include "../includes/globals.h"
#include <memory.h>
#include <string>
#include <cmath>
#include <vector>
#include "../includes/loadbmp.h"

#define PI 3.14159265359
#define toradians(x) (PI/180.0)*x

using namespace glm;
using namespace std;

GLuint loadBMP_custom(const char * imagepath);

class ObjectModel {
	public:
		mat4 modelMatrix;
		mat4 MVP;
};

class unitCube {
	public:
		GLfloat vertices[72] = {
			// front
			-1.0, -1.0,  1.0,
			1.0, -1.0,  1.0,
			1.0,  1.0,  1.0,
			-1.0,  1.0,  1.0,
			// top
			-1.0,  1.0,  1.0,
			1.0,  1.0,  1.0,
			1.0,  1.0, -1.0,
			-1.0,  1.0, -1.0,
			// back
			1.0, -1.0, -1.0,
			-1.0, -1.0, -1.0,
			-1.0,  1.0, -1.0,
			1.0,  1.0, -1.0,
			// bottom
			-1.0, -1.0, -1.0,
			1.0, -1.0, -1.0,
			1.0, -1.0,  1.0,
			-1.0, -1.0,  1.0,
			// left
			-1.0, -1.0, -1.0,
			-1.0, -1.0,  1.0,
			-1.0,  1.0,  1.0,
			-1.0,  1.0, -1.0,
			// right
			1.0, -1.0,  1.0,
			1.0, -1.0, -1.0,
			1.0,  1.0, -1.0,
			1.0,  1.0,  1.0,
		};


		GLfloat texels[2*4*6] = {
			// front
			0.0, 0.0,
			1.0, 0.0,
			1.0, 1.0,
			0.0, 1.0,
			0.0, 0.0,
			1.0, 0.0,
			1.0, 1.0,
			0.0, 1.0,
			0.0, 0.0,
			1.0, 0.0,
			1.0, 1.0,
			0.0, 1.0,
			0.0, 0.0,
			1.0, 0.0,
			1.0, 1.0,
			0.0, 1.0,
			0.0, 0.0,
			1.0, 0.0,
			1.0, 1.0,
			0.0, 1.0,
			0.0, 0.0,
			1.0, 0.0,
			1.0, 1.0,
			0.0, 1.0,
		};

		GLushort indices[36] = {
			// front
			0,  1,  2,
			2,  3,  0,
			// top
			4,  5,  6,
			6,  7,  4,
			// back
			8,  9, 10,
			10, 11,  8,
			// bottom
			12, 13, 14,
			14, 15, 12,
			// left
			16, 17, 18,
			18, 19, 16,
			// right
			20, 21, 22,
			22, 23, 20
		};
		
		GLushort backindices[36] = {
			// front
			0,  2,  1,
			2,  0,  3,
			// top
			4,  6,  5,
			6,  4,  7,
			// back
			8,  10, 9,
			10, 8,  11,
			// bottom
			12, 14, 13,
			14, 12, 15,
			// left
			16, 18, 17,
			18, 16, 19,
			// right
			20, 22, 21,
			22, 20, 23
		};
		
		GLfloat normals[72] = {
			// front
			0.0f, 0.0f, -1.0f,
			0.0f, 0.0f, -1.0f,
			0.0f, 0.0f, -1.0f,
			0.0f, 0.0f, -1.0f,
			// top
			0.0f, 1.0f, 0.0f,
			0.0f, 1.0f, 0.0f,
			0.0f, 1.0f, 0.0f,
			0.0f, 1.0f, 0.0f,
			// back
			0.0f, 0.0f, 1.0f,
			0.0f, 0.0f, 1.0f,
			0.0f, 0.0f, 1.0f,
			0.0f, 0.0f, 1.0f,
			// bottom
			0.0f, -1.0f, 0.0f,
			0.0f, -1.0f, 0.0f,
			0.0f, -1.0f, 0.0f,
			0.0f, -1.0f, 0.0f,
			// left
			-1.0f, 0.0f, 0.0f,
			-1.0f, 0.0f, 0.0f,
			-1.0f, 0.0f, 0.0f,
			-1.0f, 0.0f, 0.0f,
			// right
			1.0f, 0.0f, 0.0f,
			1.0f, 0.0f, 0.0f,
			1.0f, 0.0f, 0.0f,
			1.0f, 0.0f, 0.0f
		};

		ObjectModel cube;
		GLuint vertexbuffer,indexbuffer,colorbuffer,uvbuffer,bindexbuffer,normalbuffer;
		GLuint texture,textureUniformLocation;
		
		void setup(const char* texturefile);
		void setupCubeMap();
		void setupCubeMap(vector<imageData> imgData );
};

class unitCylinder{
	private:
		void createBFaceBuffer(){
			vec3 center;
			float theta = 0.0f;

			while ( theta < toradians(360.0f) ) {
				center = vec3(0,0,-0.5);
				bvertexbuffer.push_back(center.x);			
				bvertexbuffer.push_back(center.y);			
				bvertexbuffer.push_back(center.z);

				faceuvbuffer.push_back(0.5);
				faceuvbuffer.push_back(1.0);

				bvertexbuffer.push_back(cos(theta));
				bvertexbuffer.push_back(sin(theta));
				bvertexbuffer.push_back(-0.5);
				
				faceuvbuffer.push_back(1.0);
				faceuvbuffer.push_back(0.0);

				theta+=deltatheta;
				
				bvertexbuffer.push_back(cos(theta));
				bvertexbuffer.push_back(sin(theta));
				bvertexbuffer.push_back(-0.5);
				
				faceuvbuffer.push_back(0.0);
				faceuvbuffer.push_back(0.0);
			}	
		}
		
		void createTFaceBuffer(){
			vec3 center;
			float theta = 0.0f;

			while ( theta < toradians(360.0f) ) {
				center = vec3(0,0,-0.5);
				tvertexbuffer.push_back(center.x);			
				tvertexbuffer.push_back(center.y);			
				tvertexbuffer.push_back(center.z);

				faceuvbuffer.push_back(0.5);
				faceuvbuffer.push_back(1.0);

				tvertexbuffer.push_back(cos(theta));
				tvertexbuffer.push_back(sin(theta));
				tvertexbuffer.push_back(0.5);
				
				faceuvbuffer.push_back(1.0);
				faceuvbuffer.push_back(0.0);

				theta+=deltatheta;
				
				tvertexbuffer.push_back(cos(theta));
				tvertexbuffer.push_back(sin(theta));
				tvertexbuffer.push_back(0.5);
				
				faceuvbuffer.push_back(0.0);
				faceuvbuffer.push_back(0.0);
			}	
		}

		void createBodyBuffer(){
			float height = -0.5;
			float theta;

			while (height < 0.5){
				theta = 0.0f;

				while ( theta < toradians(360.0f)){
					bodyvertexbuffer.push_back(cos(theta));
					bodyvertexbuffer.push_back(sin(theta));
					bodyvertexbuffer.push_back(height);
					
					bodyuvbuffer.push_back(0.0);
					bodyuvbuffer.push_back(1.0);
									
					bodyvertexbuffer.push_back(cos(theta));
					bodyvertexbuffer.push_back(sin(theta));
					bodyvertexbuffer.push_back(height+deltaheight);
					
					bodyuvbuffer.push_back(0.0);
					bodyuvbuffer.push_back(0.0);
					
					bodyvertexbuffer.push_back(cos(theta+deltatheta));
					bodyvertexbuffer.push_back(sin(theta+deltatheta));
					bodyvertexbuffer.push_back(height+deltaheight);
					
					bodyuvbuffer.push_back(1.0);
					bodyuvbuffer.push_back(0.0);
		
					bodyvertexbuffer.push_back(cos(theta));
					bodyvertexbuffer.push_back(sin(theta));
					bodyvertexbuffer.push_back(height);
					
					bodyuvbuffer.push_back(0.0);
					bodyuvbuffer.push_back(1.0);
					
					bodyvertexbuffer.push_back(cos(theta+deltatheta));
					bodyvertexbuffer.push_back(sin(theta+deltatheta));
					bodyvertexbuffer.push_back(height);
					
					bodyuvbuffer.push_back(1.0);
					bodyuvbuffer.push_back(1.0);
					
					bodyvertexbuffer.push_back(cos(theta+deltatheta));
					bodyvertexbuffer.push_back(sin(theta+deltatheta));
					bodyvertexbuffer.push_back(height+deltaheight);
					
					bodyuvbuffer.push_back(1.0);
					bodyuvbuffer.push_back(0.0);

					theta+=deltatheta;
				}
			
				height+=deltaheight;									
			}
		}

	public:
		vector<GLfloat> bvertexbuffer,tvertexbuffer,faceuvbuffer,bodyvertexbuffer,bodyuvbuffer;
		float radius = 1.0f;
		float lowerheight = -0.5f;
		float deltaheight = 0.1f;
		float upperheight;
		float theta = 0.0f;
		float deltatheta = toradians(30.0f);

		float currentHeight = lowerheight;
		vec3 currentCenter = vec3(0.0f,currentHeight,0.0f);

		GLuint bvertexbufferId,tvertexbufferId,faceuvbufferId,bodyvertexbufferId,bodyuvbufferId;

		GLuint texture,textureUniformLocation;

		void setup(const char* texturefile);
};

class cuboid : public ObjectModel{
	public:
		float length,breadth,depth;
		unitCube unit;
		quat MyQuaternion;
		void createCuboid(float b, float l,float d,vec3 position,const char* texturefile);

		void renderCuboid();
		void renderSkyboxCuboid();
		void createSkyboxCuboid(float b, float l,float d,vector<imageData> imgData);
};

class cylinder : public ObjectModel{
	public:
		float radius, height;
		vec3 position;

		unitCylinder unit;
	
		void createCylinder(float radius,float h,vec3 position,const char* texturefile);
	
		void renderCylinder();
};
#endif

