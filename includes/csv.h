#include <vector>
#include <map>
#include <string>
using namespace std;
#ifndef CSV_H
#define CSV_H
//map<int, vector<map<string, int> > > transformerProperties;
//map<int, map<string, int> > globalProperties;
class keyframe{
	public:
		float timestamp;
		vector<map<string, float>> transformerProp;
		map<string,float> globalProp;
};
static vector<keyframe> keyframes;
static int nKeyframes;
static int nTransformers;
vector<keyframe> readCsv(string filename);
void print(vector<keyframe>& keyframes);
void writeCsv(vector<keyframe>& keyframes, int nTransformers, string filename);
#endif
